<?php get_header() ?>

    <section class="section-regionais">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <h1 class="section-title-01">Regionais</h1>
            <?php $regionais = archiveRegionaisQuery() ?>
            <div class="regionais">
                <?php if ($regionais->have_posts()): ?>
                    <?php while ($regionais->have_posts()): $regionais->the_post() ?>
                        <?php $permalink = get_permalink() ?>
                        <article class="regional">
                            <header>
                                <h2><?= get_the_title() ?></h2>
                            </header>
                            <ul>
                                <?php $email = get_field('email_presidente') ?>
                                <li>
                                    <h3><?= get_field('presidente') ?></h3>
                                    <div class="role">
                                        <span>Presidente</span>
                                        <a href="mailto:<?= $email ?>">
                                            <p><?= $email ?></p>
                                        </a>
                                    </div>
                                </li>
                                <?php while (have_rows('diretores')): the_row() ?>
                                    <?php while (have_rows('diretor')): the_row() ?>
                                        <?php $email = get_sub_field('email') ?>
                                        <li>
                                            <h3><?= get_sub_field('nome') ?></h3>
                                            <div class="role">
                                                <span><?= get_sub_field('cargo') ?></span>
                                                <a href="mailto:<?= $email ?>">
                                                    <p><?= $email ?></p>
                                                </a>
                                            </div>
                                        </li>
                                    <?php endwhile ?>
                                <?php endwhile ?>
                            </ul>
                            <a href="<?= $permalink ?>">Acesse a Regional</a>
                        </article>
                    <?php endwhile ?>
                <?php else: ?>
                    <div>
                        <p class="notfound-message">Não foram encontradas regionais</p>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </section>

<?php get_footer() ?>

/**
 * @description Home rd newsletter success message (after form submit)
 */
Swal.fire({
	text:
		'Olá recebemos o seu e-mail, muito obrigado por assinar a nossa newsletter!',
	showConfirmButton: true,
	confirmButtonColor: '#49a3db',
	customClass: 'sweet-alert',
	icon: 'success',
})

const formSearch = document.querySelector('nav.main-menu #search')
const textInput = formSearch.querySelector('input')

/**
 * @description Active search form
 */
formSearch.addEventListener('mouseover', () => {
	formSearch.classList.add('search-form-active')

	textInput.classList.add('search-form-active')
})

/**
 * @description Hide search form
 */
document.querySelector('*').addEventListener('click', ({ target }) => {
	const textSubmit = formSearch.querySelector('button')
	const searchImg = formSearch.querySelector('img')

	if (
		target !== formSearch &&
		target !== textInput &&
		target !== textSubmit &&
		target !== searchImg
	) {
		formSearch.classList.remove('search-form-active')
		textInput.classList.remove('search-form-active')
	}
})

const newsSlider = document.querySelector('#slider-single-comites')

/**
 * @description Slider single comitês
 */
if (newsSlider !== null) {
	tns({
		container: '#slider-single-comites',
		items: 1,
		loop: true,
		controlsText: [
			'<span class="arrow-left"></span>',
			'<span class="arrow-right"></span>',
		],
		slideBy: 'page',
		mouseDrag: true,
		touch: true,
		arrowKeys: true,
		autoplay: true,
		speed: 600,
		nav: false,
		useLocalStorage: true,
	})
}

const groupHidden = document.querySelector('input[name=comite-hidden]')
const recipHidden = document.querySelector('input[name=destinatario-hidden]')

if (groupHidden !== null && recipHidden !== null) {
	document.querySelector('input[name=comite]').value = groupHidden.value
	document.querySelector('input[name=destinatario]').value = recipHidden.value
}

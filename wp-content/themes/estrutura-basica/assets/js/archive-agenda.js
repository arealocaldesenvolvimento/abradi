/**
 * @description default calendar settings
 */
const calendarSettings = {
	animate: true,
	classNames: 'calendar',
	closeButton: false,
	dateFormat: 'dd/mm/YYYY',
	locale: 'pt_BR',
	timeFormat: false,
	timeHours: false,
	timeMinutes: false,
	timeSeconds: false,
}

/**
 * @description calendar events "from", "to"
 */
tail.DateTime('.calendar-01', calendarSettings)
tail.DateTime('.calendar-02', calendarSettings)

/**
 * @description responsive handle
 */
if (screen.width <= 768) {
	document
		.querySelector('.eventos-recentes article.highlight')
		.classList.remove('highlight')
}

// /**
//  * @description load more events
//  */
// document
// 	.querySelector('.load-more-eventos nav a')
// 	.addEventListener('click', async event => {
// 		event.preventDefault()

// 		const link = event.target.href
// 		event.target.innerHTML = 'Carregando...'

// 		const response = await fetch(link)
// 		const nextPage = await response.text()

// 		const parser = new DOMParser()
// 		const nextPageHtml = parser.parseFromString(nextPage, 'text/html')

// 		const newEvents = nextPageHtml.querySelectorAll(
// 			'.section-eventos > .al-small-container > .eventos-recentes article'
// 		)

// 		const currentPage = document.querySelector(
// 			'.section-eventos > .al-small-container > .eventos-recentes'
// 		)

// 		newEvents.forEach(event => {
// 			event.classList.remove('highlight')
// 			currentPage.append(event)
// 		})

// 		const nextPageLoadMore = nextPageHtml.querySelector(
// 			'.load-more-eventos nav a'
// 		)

// 		if (nextPageLoadMore !== null) {
// 			event.target.innerHTML = 'Carregar Mais!'
// 			event.target.href = nextPageLoadMore.href
// 		} else {
// 			event.target.remove()
// 		}
// 	})

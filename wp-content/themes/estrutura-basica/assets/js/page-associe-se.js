const memberForm = document.querySelector('section.section-associe-se form[name=form-associe-se]')
const submit = memberForm.querySelector('input[type=submit]')
const previousButton = memberForm.querySelector('button[name=previous]')
const nextButton = memberForm.querySelector('button[name=next]')

/**
 * @description verrifies the current group of inputs
 */
function checkInputs(group = 1) {
	let inputs = []

	memberForm.querySelectorAll(`div.group-${group} input`).forEach(input => {
		inputs = [...inputs, input.value]

		input.addEventListener('change', () => checkInputs(group))
	})

	if (inputs.includes('') || inputs.includes(null)) {
		nextButton.disabled = true
	} else {
		nextButton.disabled = false
	}

	if ((inputs.includes('') || inputs.includes(null)) && group === 4) {
		submit.disabled = true
	} else {
		submit.disabled = false
	}

	if (group === 4) {
		nextButton.disabled = true
		nextButton.classList.add('inactive')
		nextButton.classList.remove('active')
		submit.classList.remove('inactive')
		submit.classList.add('active')
	} else {
		submit.classList.add('inactive')
		submit.classList.remove('active')
		nextButton.classList.remove('inactive')
		nextButton.classList.add('active')
	}
}

/**
 * @description get a div.group
 */
const getFormGroup = value => memberForm.querySelector(`div.group-${value}`)

/**
 * @description disable buttons "voltar" and/or "avançar"
 */
function disableButtons() {
	switch (Number(memberForm.querySelector('input[name=currentGroup]').value)) {
	case 1:
		previousButton.disabled = true
		nextButton.disabled = false
		return

	case 4:
		nextButton.disabled = true
		previousButton.disabled = false
		return

	default:
		previousButton.disabled = false
		nextButton.disabled = false
		return
	}
}

/**
 * @description render the next form input group on screen
 */
previousButton.addEventListener('click', () => {
	let value = memberForm.querySelector('input[name=currentGroup]').value
	getFormGroup(value).classList.remove('current')

	value = memberForm.querySelector('input[name=currentGroup]').value = -- value

	getFormGroup(value).classList.add('current')
	disableButtons()

	checkInputs(value)
})

/**
 * @description Volta para o grupo de inputs anterior
 */
nextButton.addEventListener('click', () => {
	let value = memberForm.querySelector('input[name=currentGroup]').value
	getFormGroup(value).classList.remove('current')

	value = memberForm.querySelector('input[name=currentGroup]').value = ++ value

	getFormGroup(value).classList.add('current')
	disableButtons()

	checkInputs(value)
})

/**
 * @description change "tipo da empresa" field value
 */
memberForm.querySelector('select[name=tipo]').addEventListener('change', event => {
	const typeSwitch = typeValue => ({
		0: 'Agência Digital',
		1: 'Produtora Digital',
		2: 'Agente Digital',
		3: 'Colaborador',
		4: 'Agente Apoiador',
	})[typeValue]

	memberForm.querySelector('input[name=tipoEmpresa]').value = typeSwitch(event.target.value)
})

/**
 * @description change "mensalidade" and "faixa de faturamento" field values
 */
memberForm.querySelector('select[name=faixaFaturamento]').addEventListener('change', event => {
	const rangeSwitch = rangeValue => ({
		0: 'Grupo 1 - R$ 108',
		1: 'Grupo 2 - R$ 129',
		2: 'Grupo 3 - R$ 162',
		3: 'Grupo 4 - R$ 194',
		4: 'Grupo 5 - R$ 270',
		5: 'Grupo 6 - R$ 400',
		6: 'Grupo 7 - R$ 600',
		7: 'Grupo 8 - R$ 750',
	})[rangeValue]

	memberForm.querySelector('input[name=mensalidade]').value = rangeSwitch(event.target.value)
})

/**
 * @description form validation
 */
memberForm.querySelector('input[name=cnpj]').addEventListener('focusout', ({ target }) => {
	if (!validateCnpj(target.value)) {
		target.classList.add('input-danger')
		target.classList.remove('input-success')
		submit.disabled = true
		nextButton.disabled = true
	} else {
		target.classList.add('input-success')
		target.classList.remove('input-danger')
		submit.disabled = false
		nextButton.disabled = false
	}
})

memberForm.querySelector('input[name=representanteLegal]').addEventListener('focusout', ({ target }) => {
	if ((target.value.indexOf(' ') < 1 || !target.value.search(/[^a-zA-Z]+/))) {
		target.classList.add('input-danger')
		target.classList.remove('input-success')
		submit.disabled = true
	} else {
		target.classList.add('input-success')
		target.classList.remove('input-danger')
		submit.disabled = false
	}
})

/**
 * @description inputs mask for form fields
 */
IMask(memberForm.querySelector('input[name=cnpj]'), { mask: '00.000.000/0000-00' })
IMask(memberForm.querySelector('input[name=cep]'), { mask: '00000-000' })
IMask(memberForm.querySelector('input[name=telefone]'), { mask: '(00) 0000-0000' })
IMask(memberForm.querySelector('input[name=celular]'), { mask: '(00) 00000-0000' })

/**
 * @description fetch all states and cities to select
 */
async function getStatesAndCities() {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { estados } = await stateResponse.json()
	const selectUf = memberForm.querySelector('select[name=estado]')

	estados.map(estado => selectUf.options[selectUf.options.length] = new Option(estado.uf, estado.id))

	selectUf.addEventListener('change', async event => {
		const formData = new FormData()
		formData.append('estado', event.target.value)

		const cityResponse = await fetch(`${window.apiUrl}/cidades`, {
			method: 'POST',
			body: formData
		})

		const { cidades } = await cityResponse.json()
		const cidadesJson = JSON.parse(cidades)
		const selectCidades = memberForm.querySelector('select[name=cidade]')

		selectCidades.innerHTML = ''

		cidadesJson.map(cidade =>
			selectCidades.options[selectCidades.options.length] = new Option(cidade.nome, cidade.nome)
		)
	})
}

/**
 * @description submit a new member data and handle the responses
 */
async function handleFormSubmit() {
	memberForm.addEventListener('submit', async event => {
		event.preventDefault()

		const formData = {}

		Object
			.values(event.srcElement.elements)
			.map(input => formData[input.name] = input.value)

		const selectUf = memberForm.querySelector('select[name=estado]')
		formData.estado = selectUf.options[selectUf.selectedIndex].text

		delete formData['currentGroup']
		delete formData['faixaFaturamento']
		delete formData['tipo']
		delete formData['previous']
		delete formData['next']

		Swal.fire({
			title: 'Enviando...',
			timerProgressBar: true,
			customClass: 'sweet-alert',
			onBeforeOpen: () => {
				Swal.showLoading()
			},
		})

		const memberResponse = await fetch(`${window.apiUrl}/associe-se`, {
			method: 'POST',
			body: JSON.stringify(formData),
			headers : {
				'Content-Type': 'application/json',
			}
		})

		const { message, status } = await memberResponse.json()

		const alertConfigs = {
			title: message,
			showConfirmButton: true,
			confirmButtonColor: '#49a3db',
			customClass: 'sweet-alert',
		}

		if (status !== 201) {
			alertConfigs.icon = 'error'
		} else {
			alertConfigs.icon = 'success'
		}

		Swal.fire(alertConfigs)
	})
}

checkInputs()
getStatesAndCities()
handleFormSubmit()

function validateCnpj(cnpj) {
	if (!cnpj)
		return false

	cnpj = cnpj.replace(/[^\d]+/g, '')

	let size = cnpj.length - 2
	let number = cnpj.substring(0, size)
	let digits = cnpj.substring(size)
	let sum = 0
	let pos = size - 7

	for (let i = size; i >= 1; i--) {
		sum += number.charAt(size - i) * pos--
		if (pos < 2)
			pos = 9
	}
	let result = sum % 11 < 2 ? 0 : 11 - sum % 11

	if (result != digits.charAt(0))
		return false

	size = size + 1
	number = cnpj.substring(0, size)
	sum = 0
	pos = size - 7

	for (let i = size; i >= 1; i--) {
		sum += number.charAt(size - i) * pos--
		if (pos < 2)
			pos = 9
	}

	result = sum % 11 < 2 ? 0 : 11 - sum % 11

	if (result != digits.charAt(1))
		return false

	return true
}

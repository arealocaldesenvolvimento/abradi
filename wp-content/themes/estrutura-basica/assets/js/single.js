/**
 * @description Slider single notícias
 */
tns({
	container: '#slider-single-noticias',
	items: 1,
	loop: true,
	controlsText: [
		'<span class="arrow-left"></span>',
		'<span class="arrow-right"></span>',
	],
	slideBy: 'page',
	mouseDrag: true,
	touch: true,
	arrowKeys: true,
	autoplay: true,
	speed: 600,
	nav: false,
	useLocalStorage: true,
})

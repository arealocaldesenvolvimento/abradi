const regionalSlider = document.querySelector('#slider-regionais')
const newsSlider = document.querySelector('#slider-noticias')
const contributorsSlider = document.querySelector('#slider-patrocinadores')

/**
 * @description regionais slider
 */
if (regionalSlider !== null) {
	tns({
		container: '#slider-regionais',
		items: 1,
		loop: true,
		slideBy: 'page',
		mouseDrag: true,
		touch: true,
		arrowKeys: false,
		autoplay: true,
		speed: 600,
		nav: false,
		useLocalStorage: true,
	})
}

/**
 * @description news slider
 */
if (newsSlider !== null) {
	tns({
		container: '#slider-noticias',
		items: 1,
		loop: true,
		controlsText: [
			'<span class="arrow-left"></span>',
			'<span class="arrow-right"></span>',
		],
		slideBy: 'page',
		mouseDrag: true,
		touch: true,
		arrowKeys: true,
		autoplay: true,
		speed: 600,
		nav: false,
		useLocalStorage: true,
		responsive: {
			480: {
				gutter: 26,
			},
		},
	})
}

/**
 * @description contributors slider
 */
if (contributorsSlider !== null) {
	tns({
		container: '#slider-patrocinadores',
		items: 1,
		loop: true,
		mouseDrag: true,
		touch: true,
		arrowKeys: false,
		controls: false,
		nav: false,
		autoplay: true,
		speed: 600,
		responsive: {
			1024: {
				items: 4,
			},
			769: {
				items: 3,
			},
			480: {
				items: 2,
				gutter: 50,
			},
		},
	})
}

/**
 * @description black/white imgs hover effect
 */
document.querySelectorAll('.patrocinador img').forEach(img => {
	const originalImage = img.getAttribute('src')

	img.addEventListener('mouseover', ({ target }) => {
		const blackWhiteImage = target.getAttribute('data-image')

		if (blackWhiteImage !== null) {
			target.setAttribute('src', blackWhiteImage)
		}
	})

	img.addEventListener('mouseleave', ({ target }) => {
		target.setAttribute('src', originalImage)
	})
})

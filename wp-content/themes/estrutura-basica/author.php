<?php

get_header();

$user = (isset($_GET['author_name']))
    ? get_user_by('slug', $author_name)
    : get_userdata(intval($author));

$title = get_user_meta($user->ID, 'empresa_membro', true) ?? $user->display_name;

$location = get_user_meta($user->ID, 'localizacao_membro', true);
$address = get_user_meta($user->ID, 'endereco_membro', true);
$video = get_user_meta($user->ID, 'video_membro', true);

?>

    <section class="section-single-agentes-digitais">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <h1 class="section-title-01"><?= $title ?></h1>
            <?php if ($segments = get_user_meta($user->ID, 'area_atuacao', true)): ?>
                <?php foreach ($segments as $post): ?>
                    <span><?= get_the_title($post) ?></span>
                <?php endforeach ?>
            <?php endif ?>
            <div class="content">
                <div class="text">
                    <div class="logo" <?= !$video ? 'style="text-align: center;"' : '' ?>>
                        <?php if ($logo = get_user_meta($user->ID, 'logomarca_membro', true)): ?>
                            <?php if (is_array($logo)): ?>
                                <?= wp_get_attachment_image($logo['ID'], 'medium') ?>
                            <?php elseif (wp_get_attachment_image($logo, 'medium')): ?>
                                <?= wp_get_attachment_image($logo, 'medium') ?>
                            <?php else: ?>
                                <img
                                    src="<?= get_image_url('default-thumbnail.jpg') ?>"
                                    alt="Imagem destacada padrão para postagens"
                                />
                            <?php endif ?>
                        <?php else: ?>
                            <img
                                src="<?= get_image_url('default-thumbnail.jpg') ?>"
                                alt="Imagem destacada padrão para postagens"
                            />
                        <?php endif ?>
                    </div>
                    <p>
                        <?= wpautop(sanitizeString(get_user_meta($user->ID, 'descricao_membro', true))) ?>
                    </p>
                </div>
                <div class="video">
                    <?php if ($video): ?>
                        <iframe src="<?= $video ?>"></iframe>
                    <?php endif ?>
                </div>
            </div>
            <?php if ($sobre = get_user_meta($user->ID, 'sobre_membro', true)): ?>
                <div class="competencias">
                    <h2 class="section-title-01">Competências</h2>
                    <div class="competencia">
                        <p><?= wpautop($sobre) ?></p>
                    </div>
                </div>
            <?php endif ?>
            <?php
                $contactInfos = [
                    'envelope-azul' => get_user_meta($user->ID, 'email_membro', true),
                    'facebook-azul' => get_user_meta($user->ID, 'facebook', true),
                    'aperto-de-maos-azul' => get_user_meta($user->ID, 'gerente_membro', true),
                    'instagram-azul' => get_user_meta($user->ID, 'instagram', true),
                    'telefone-azul' => get_user_meta($user->ID, 'telefone_membro', true),
                    'linkedin-azul' => get_user_meta($user->ID, 'linkedin', true),
                    'globo-azul' => $user->user_url,
                ];
            ?>
            <?php if (!empty(array_filter($contactInfos))): ?>
                <div class="contato">
                    <h2 class="section-title-01">Informações de Contato</h2>
                    <div>
                        <?php foreach ($contactInfos as $key => $info): ?>
                            <?php if ($info): ?>
                                <div class="informacao">
                                    <p class="<?= $key ?>">
                                        <img
                                            src="<?= get_image_url('single-convenios/' . $key . '.svg') ?>"
                                            alt="Informação de Contato"
                                        />
                                        <?php if ($key === 'globo-azul' || $key === 'linkedin-azul' || $key === 'facebook-azul' || $key === 'instagram-azul'): ?>
                                            <a href="<?= $info ?>" target="_blank" title="<?= $title ?>">
                                                <?= removeSlashBar($info) ?>
                                            </a>
                                        <?php elseif ($key === 'envelope-azul'): ?>
                                            <a href="mailto:<?= $info ?>" target="_blank" title="<?= $title ?>">
                                                <?= removeSlashBar($info) ?>
                                            </a>
                                        <?php elseif ($key === 'telefone-azul'): ?>
                                            <?php $unformPhone = str_replace(' ', '', preg_replace("/[^a-zA-Z0-9\s]/", '', $info)) ?>
                                            <a
                                                href="https://api.whatsapp.com/send?phone=<?= $unformPhone ?>"
                                                target="_blank"
                                                title="<?= $title ?>"
                                            >
                                                <?= removeSlashBar($info) ?>
                                            </a>
                                        <?php else: ?>
                                            <?= removeSlashBar($info) ?>
                                        <?php endif ?>
                                    </p>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    </div>
                </div>
            <?php endif ?>
            <?php if ($location && $address): ?>
                <div class="localizacao">
                    <h2 class="section-title-01">Localização</h2>
                    <div class="infos">
                        <p>
                            <img
                                src="<?= get_image_url('single-eventos/maps-azul.svg') ?>"
                                alt="Localização"
                            />
                            <?= $address ?>
                        </p>
                        <a
                            href="http://maps.google.com/?q=<?= $address ?>"
                            title="Como Chegar?"
                            target="_blank"
                        >
                            Como Chegar?
                        </a>
                    </div>
                    <div class="mapa">
                        <input class="lat" type="hidden" value="<?= $location['lat'] ?>">
                        <input class="lng" type="hidden" value="<?= $location['lng'] ?>">
                        <section id="cd-google-map">
                            <div id="google-container"></div>
                            <div id="cd-zoom-in"></div>
                            <div id="cd-zoom-out"></div>
                        </section>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/author.js" defer></script>

<?php get_footer() ?>


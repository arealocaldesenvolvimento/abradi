<?php

get_header();

$regional = get_post($_SESSION['regional']);
$regionalSlug = $regional->post_name;
$regionalTitle = $regional->post_title;

/**
 * Layer-slider plugin IDs
 */
$sliderIds = [
    'nacional' => 1,
    'df' => 2,
    'go' => 3,
    'ms' => 4,
    'norte' => 5,
    'pb' => 6,
    'pr' => 7,
    'rj' => 8,
    'rn' => 9,
    'rs' => 10,
    'sc' => 11,
    'sp' => 12,
];

?>

    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css"
        rel="stylesheet"
    >
    <div class="mobile-logo">
        <img
            src="<?= get_field('logo', $_SESSION['regional'])['sizes']['medium'] ?>"
            alt="Logo <?= $regionalTitle ?>"
        />
    </div>
    <?php layerslider($sliderIds[$regionalSlug]) ?>
    <div class="section-home" role="main">
        <section class="section-destaques">
            <div class="al-small-container">
                <h1 class="section-title-01">Destaques</h1>
                <?php $highlights = singleRegionaisHighlightsQuery($regionalSlug) ?>
                <?php if ($highlights->have_posts()): ?>
                    <div class="destaques">
                        <?php $count = 1 ?>
                        <?php while ($highlights->have_posts()): $highlights->the_post() ?>
                            <?php
                                $isSmallCard = $highlights->post_count >= 5 && $count >= 3;
                                $title = $isSmallCard
                                    ? wp_trim_words(get_the_title(), 6)
                                    : wp_trim_words(get_the_title(), 8);
                            ?>
                            <article class="<?= $isSmallCard ? 'small' : '' ?>">
                                <a href="<?= get_permalink() ?>">
                                    <p class="ases-text"><?= $title ?></p>
                                    <div class="image-wrapper">
                                        <?= setPostThumbnail('large') ?>
                                    </div>
                                    <div class="content">
                                        <h2><?= $title ?></h2>
                                        <span><?= get_the_date() ?></span>
                                    </div>
                                </a>
                            </article>
                        <?php $count ++; endwhile ?>
                    </div>
                <?php else: ?>
                    <p class="notfound-message">Não foram cadastrados destaques</p>
                <?php endif ?>
            </div>
        </section>
        <section class="section-seja-um-associado">
            <div class="al-small-container">
                <h2 class="section-title-02">
                    Seja um <span>Associado!</span>
                </h2>
                <div class="brand">
                    <p>Faça parte desta rede de inovação e relacionamento e contribua para o crescimento do setor!</p>
                    <a href="<?= get_permalink(38) ?>">Saiba Mais!</a>
                </div>
                <div class="icons">
                    <div class="icon">
                        <img
                            src="<?= get_image_url('home/network.svg') ?>"
                            alt="Network"
                        />
                        <span>Network</span>
                    </div>
                    <div class="icon">
                        <img
                            src="<?= get_image_url('home/livros.svg') ?>"
                            alt="Conhecimento"
                        />
                        <span>Conhecimento</span>
                    </div>
                    <div class="icon">
                        <img
                            src="<?= get_image_url('home/time.svg') ?>"
                            alt="Representatividade"
                        />
                        <span>Representatividade</span>
                    </div>
                    <div class="icon">
                        <img
                            src="<?= get_image_url('home/medalha.svg') ?>"
                            alt="Benefícios"
                        />
                        <span>Benefícios</span>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-noticias">
            <div class="al-small-container">
                <h2 class="section-title-01">Notícias</h2>
                <?php $news = singleRegionaisNewsQuery($regionalSlug) ?>
                <?php if ($news->have_posts()): ?>
                    <div id="slider-noticias">
                        <?php while ($news->have_posts()): $news->the_post() ?>
                            <?php $title = get_the_title() ?>
                            <article>
                                <a href="<?= get_permalink() ?>" class="noticia">
                                    <p class="ases-text"><?= $title ?></p>
                                    <div class="image-wrapper">
                                        <?= setPostThumbnail('large') ?>
                                    </div>
                                    <div class="content">
                                        <h3><?= wp_trim_words($title, 8) ?></h3>
                                        <span><?= get_the_date() ?></span>
                                    </div>
                                </a>
                            </article>
                        <?php endwhile ?>
                    </div>
                <?php else: ?>
                    <p class="notfound-message">Não foram cadastrados notícias</p>
                <?php endif ?>
            </div>
        </section>
        <section class="section-eventos">
            <div class="al-small-container">
                <div class="brand">
                    <h2 class="section-title-01">Eventos</h2>
                </div>
                <?php $events = singleRegionaisEventsQuery($regionalSlug) ?>
                <?php if ($events->have_posts()): ?>
                     <div class="eventos">
                        <?php while ($events->have_posts()): $events->the_post() ?>
                            <?php $title = get_the_title() ?>
                            <article>
                                <a href="<?= get_permalink() ?>" class="evento">
                                    <p class="ases-text"><?= $title ?></p>
                                    <div class="image-wrapper">
                                        <?php if (get_the_post_thumbnail_url()): ?>
                                            <img
                                                src="<?= get_the_post_thumbnail_url(null, GLOBALS['THUMB_SIZE']) ?>"
                                                alt="<?= get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ?>"
                                            />
                                        <?php else: ?>
                                            <img
                                                src="<?= get_image_url('default-event-thumbnail.jpg') ?>"
                                                alt='Imagem destacada padrão para eventos ABRADi'
                                            />
                                        <?php endif ?>
                                    </div>
                                    <div class="content">
                                        <h3><?= wp_trim_words($title, 8) ?></h3>
                                        <span>
                                            <?= date('d/m/Y', strtotime(get_field('data'))) ?>
                                        </span>
                                    </div>
                                </a>
                            </article>
                        <?php endwhile ?>
                    </div>
                <?php else: ?>
                    <p class="notfound-message">Não foram cadastrados eventos</p>
                <?php endif ?>
            </div>
        </section>
        <section class="section-patrocinadores">
            <div class="al-small-container">
                <h2 class="section-title-01">Colaboradores</h2>
                <?php $contributors = singleRegionaisContributorsQuery($regionalSlug) ?>
                <?php if ($contributors->have_posts()): ?>
                    <div id="slider-patrocinadores">
                        <?php while ($contributors->have_posts()): $contributors->the_post() ?>
                            <?php $title = get_the_title() ?>
                            <a
                                class="patrocinador"
                                href="<?= get_field('link') ?>"
                                target="_blank"
                            >
                                <p class="ases-text"><?= $title ?></p>
                                <img
                                    src="<?= get_the_post_thumbnail_url(null, GLOBALS['THUMB_SIZE']) ?>"
                                    alt="<?= get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ?>"
                                    <?php if (!empty($image = get_field('imagem_preto_branco'))): ?>
                                        data-image="<?= $image['sizes']['medium'] ?>"
                                    <?php endif ?>
                                />
                            </a>
                        <?php endwhile ?>
                    </div>
                <?php else: ?>
                    <div>
                        <p class="notfound-message">Não foram cadastrados colaboradores</p>
                    </div>
                <?php endif ?>
            </div>
        </section>
        <?php
            $states = GLOBALS['STATES'];
            unset($states['Nacional'], $states['Norte']);
        ?>
        <section class="section-newsletter">
            <div class="al-small-container">
                <div class="brand">
                    <h2>Gostou do Conteúdo?</h2>
                    <p>Cadastre seu e-mail e</p>
                    <p>fique por dentro de nossas novidades.</p>
                </div>
                <div class="form-container">
                    <form
                        id="conversion-form-newsletter-home-regionais"
                        action="https://www.rdstation.com.br/api/1.2/conversions"
                        data-typed-fields=""
                        data-lang="pt-BR"
                    >
                        <fieldset form="conversion-form-newsletter-home-regionais">
                            <div>
                                <input
                                    type="hidden"
                                    name="token_rdstation"
                                    value="<?= KEYS['RDSTATION_API_TOKEN'] ?>"
                                >
                                <input
                                    type="hidden"
                                    name="identificador"
                                    value="newsletter-home-regionais"
                                >
                                <input
                                    type="hidden"
                                    name="_is"
                                    value="6"
                                >
                                <input
                                    type="hidden"
                                    name="c_utmz"
                                    value=""
                                >
                                <input
                                    type="hidden"
                                    name="traffic_source"
                                    value=""
                                >
                                <input
                                    type="hidden"
                                    name="client_id"
                                    value=""
                                >
                                <input
                                    type="hidden"
                                    name="redirect_to"
                                    value="<?= home_url('?message=success') ?>"
                                >
                                <input
                                    type="hidden"
                                    name="_doe"
                                    value=""
                                >
                                <input
                                    type="hidden"
                                    name="thankyou_message"
                                    value="Obrigado!"
                                    disabled=""
                                >
                                <input
                                    type="hidden"
                                    name="emP7yF13ld"
                                    value=""
                                >
                                <input
                                    type="hidden"
                                    name="sh0uldN07ch4ng3"
                                    value="should_not_change"
                                >
                                <label for="rd-email_field-k8qa9r95">
                                    <input
                                        id="rd-email_field-k8qa9r95"
                                        name="email"
                                        type="email"
                                        data-use-type="STRING"
                                        placeholder="Email *"
                                        required
                                    >
                                </label>
                                <select
                                    id="rd-select_field-k91ew689"
                                    class="bricks-form__input required"
                                    data-use-type="STRING"
                                    name="uf"
                                >
                                    <optgroup label="Estado">
                                        <?php foreach ($states as $state): ?>
                                            <option value="<?= $state ?>">
                                                <?= $state ?>
                                            </option>
                                        <?php endforeach ?>
                                    </optgroup>
                                </select>
                                <input
                                    type="submit"
                                    id="cf_submit"
                                    value="Enviar"
                                    class="g-recaptcha"
                                    data-sitekey="<?= KEYS['GOOGLE_CAPTCHA_HOME_KEY'] ?>"
                                    data-callback='onSubmit'
                                />
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </section>
    </div>

    <script src="<?= get_template_directory_uri() ?>/public/tiny-slider.js"></script>
    <script src="<?= get_template_directory_uri() ?>/public/google-recaptcha.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="<?= get_template_directory_uri() ?>/public/single-regionais.js" defer></script>

    <?php if ($_GET['message'] === 'success'): ?>
        <script src="<?= get_template_directory_uri() ?>/public/sweetalert2@9.js"></script>
        <script src="<?= get_template_directory_uri() ?>/public/single-regionais-newsletter.js" defer></script>
    <?php endif ?>

<?php get_footer() ?>

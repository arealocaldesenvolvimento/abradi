<?php

if (is_singular('regionais'))
    $_SESSION['regional'] = get_queried_object_id();
else if (is_home())
    $_SESSION['regional'] = GLOBALS['ID_NACIONAL'];

?>
<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
    <?php if ($_SESSION['regional'] === GLOBALS['ID_NACIONAL']): ?>
        <?= get_field('google_analytics', $_SESSION['regional']) ?>
    <?php else: ?>
        <?= get_field('google_analytics', GLOBALS['ID_NACIONAL']) ?>
        <?= get_field('google_analytics', $_SESSION['regional']) ?>
    <?php endif ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#49a3db">
    <meta name="web_author" content="Área Local">
    <link rel="icon" href="<?php image_url('favicon.png') ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url') ?>">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <?php
        global $template;

        $page = str_replace('.php', '', basename($template));
        $page === 'index' ? $page = 'single-regionais' : '';
    ?>
    <link
        rel="stylesheet"
        href="<?= get_template_directory_uri() . '/public/' . $page . '.css' ?>"
    >
    <title><?= wp_title() ?></title>
    <?php wp_head() ?>
</head>
<body <?php body_class() ?>>
    <?php
        $regionais = new WP_Query([
            'post_type' => 'regionais',
            'posts_per_page' => -1,
            'post__in' => [
                352,
                9616,
                9619,
                9623,
                9738,
                9625,
                9331,
                9627,
                10038,
                9631,
                9329,
                9633
            ],
            'order' => 'ASC',
            'orderby'=>'post__in',
        ])
    ?>
    <header class="main-header">
        <div class="first-row">
            <div class="al-small-container">
                <nav class="menu-01">
                    <?php
                        wp_nav_menu([
                            'menu' => 'Header01',
                            'theme_location' => 'Header',
                            'container' => false
                        ])
                    ?>
                </nav>
                <nav class="menu-02">
                    <?php
                        wp_nav_menu([
                            'menu' => 'Header02',
                            'theme_location' => 'Header',
                            'container' => false
                        ])
                    ?>
                </nav>
            </div>
        </div>
        <div class="second-row">
            <div class="al-small-container">
                <nav class="regionais-menu">
                    <div class="toggle-regionais-menu">
                        <div class="toggle">
                            <span>Selecione uma regional</span>
                            <img
                                src="<?= get_image_url('header/flecha-direita.svg') ?>"
                                alt="Flecha para a direita"
                            />
                            <ul>
                                <?php while ($regionais->have_posts()): $regionais->the_post() ?>
                                    <li>
                                        <a href="<?= get_permalink() ?>">
                                            <?= get_the_title() ?>
                                        </a>
                                    </li>
                                <?php endwhile ?>
                            </ul>
                        </div>
                    </div>
                    <a href="<?= get_home_url('/') ?>">
                        <img
                            src="<?= get_field('logo', $_SESSION['regional'])['sizes']['medium'] ?>"
                            title="Abradi"
                            alt="Logo Abradi"
                        />
                    </a>
                </nav>
                <nav class="main-menu">
                    <?php
                        wp_nav_menu([
                            'menu' => 'Header03',
                            'theme_location' => 'Header',
                            'container' => false
                        ])
                    ?>
                    <?= get_search_form() ?>
                </nav>
            </div>
        </div>
    </header>
    <?php if (wp_is_mobile()): ?>
        <header class="main-mobile-header">
            <span>Selecione uma regional</span>
            <ul>
                <?php while ($regionais->have_posts()): $regionais->the_post() ?>
                    <li>
                        <a href="<?= get_permalink() ?>">
                            <?= get_the_title() ?>
                        </a>
                    </li>
                <?php endwhile ?>
            </ul>
        </header>
    <?php endif ?>
    <div id="wrapper">

<?php get_header() ?>

    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css"
        rel="stylesheet"
    >
    <section class="section-single-noticias">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <div class="noticia">
                <?php while (have_posts()): the_post() ?>
                    <h1 class="section-title-01"><?= get_the_title() ?></h1>
                    <span><?= get_the_date() ?></span>
                    <?= setPostThumbnail('large') ?>
                    <div class="content">
                        <p><?php the_content() ?></p>
                    </div>
                <?php endwhile ?>
            </div>
            <div class="share-links">
                <?= do_shortcode('[addtoany]') ?>
            </div>
            <div class="slider-single-noticias-container">
                <h2 class="section-title-01">Fique por dentro!</h2>
                <div id="slider-single-noticias">
                    <?php $relatedNews = singlePageQuery(get_post($_SESSION['regional'])->post_name) ?>
                    <?php while ($relatedNews->have_posts()): $relatedNews->the_post() ?>
                        <?php $permalink = get_permalink() ?>
                        <div class="item">
                            <a href="<?= $permalink ?>">
                                <?= setPostThumbnail() ?>
                            </a>
                            <div class="content">
                                <a href="<?= $permalink ?>">
                                    <h3>
                                        <?= wp_trim_words(get_the_title(), 8) ?>
                                    </h3>
                                </a>
                                <a href="<?= $permalink ?>">
                                    <p><?= sanitizeString(get_the_excerpt()) ?></p>
                                </a>
                                <a href="<?= $permalink ?>">
                                    <span><?= get_the_date() ?></span>
                                </a>
                            </div>
                        </div>
                    <?php endwhile ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/tiny-slider.js"></script>
    <script src="<?= get_template_directory_uri() ?>/public/single.js" defer></script>

<?php get_footer() ?>


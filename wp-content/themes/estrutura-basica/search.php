<?php get_header(); $search = get_search_query() ?>

    <section class="section-search">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <h1 class="section-title-01">
                Resultados para <span><?= $search ?></span>
            </h1>
            <?php $search = searchQuery($search) ?>
            <?php if ($search->have_posts()): ?>
                <div class="results">
                    <?php while ($search->have_posts()): $search->the_post() ?>
                        <?php $permalink = get_permalink() ?>
                        <article>
                            <a href="<?= $permalink ?>">
                                <?= setPostThumbnail() ?>
                            </a>
                            <div class="content">
                                <a href="<?= $permalink ?>">
                                    <h2><?= wp_trim_words(get_the_title(), 8) ?></h2>
                                </a>
                                <a href="<?= $permalink ?>">
                                    <span><?= get_the_date() ?></span>
                                </a>
                            </div>
                        </article>
                    <?php endwhile ?>
                </div>
                <div>
                    <nav class="pagination">
                        <?= searchPaginationLinks() ?>
                    </nav>
                </div>
            <?php else: ?>
                <div>
                    <p class="notfound-message">
                        Desculpe, mas nada foi encontrado para sua busca
                    </p>
                </div>
            <?php endif ?>
        </div>
    </section>

<?php get_footer() ?>

    </div>

    <footer class="main-footer">
        <div class="al-small-container">
            <a href="<?= get_home_url('/') ?>">
                <p class="ases-text">Abradi</p>
                <img
                    src="<?= get_image_url('footer/logo-abradi-nacional-footer.png') ?>"
                    alt="Abradi"
                />
            </a>
            <div class="menu-01">
                <p>Abradi</p>
                <?php
                    wp_nav_menu([
                        'menu' => 'Footer01',
                        'theme_location' => 'Footer',
                        'container' => false
                    ])
                ?>
            </div>
            <div class="menu-02">
                <p>Associados</p>
                <?php
                    wp_nav_menu([
                        'menu' => 'Footer02',
                        'theme_location' => 'Footer',
                        'container' => false
                    ])
                ?>
            </div>
            <div class="menu-03">
                <p>Conteúdo</p>
                <?php
                    wp_nav_menu([
                        'menu' => 'Footer03',
                        'theme_location' => 'Footer',
                        'container' => false
                    ])
                ?>
            </div>
            <div class="menu-04">
                <p>Contato</p>
                <?php
                    wp_nav_menu([
                        'menu' => 'Footer04',
                        'theme_location' => 'Footer',
                        'container' => false
                    ])
                ?>
                <div class="social">
                    <a
                        href="https://pt-br.facebook.com/abradinacional/"
                        target="_blank"
                    >
                        <p class="ases-text">Facebook</p>
                        <img
                            src="<?= get_image_url('footer/facebook.svg') ?>"
                            alt="Logo Facebook"
                        />
                    </a>
                    <a
                        href="https://www.instagram.com/abradi_nacional/"
                        target="_blank"
                    >
                        <p class="ases-text">Instagram</p>
                        <img
                            src="<?= get_image_url('footer/instagram.svg') ?>"
                            alt="Logo Instagram"
                        />
                    </a>
                    <a
                        href="https://br.linkedin.com/company/abradi1"
                        target="_blank"
                    >
                        <p class="ases-text">Linkedin</p>
                        <img
                            src="<?= get_image_url('footer/linkedin.svg') ?>"
                            alt="Logo Linkedin"
                        />
                    </a>
                    <a
                        href="https://www.youtube.com/channel/UCMFZeCN-OYwUlc77bGA_XaQ"
                        target="_blank"
                    >
                        <p class="ases-text">Youtube</p>
                        <img
                            src="<?= get_image_url('footer/youtube.svg') ?>"
                            alt="Logo Youtube"
                        />
                    </a>
                </div>
            </div>
        </div>
        <div class="area">
            <span>Desenvolvido por:</span>
            <a href="https://www.arealocal.com.br" target="_blank">
                Área Local
            </a>
        </div>
    </footer>

    <script>
        window.alUrl = {
            templateUrl: '<?= addslashes(get_bloginfo('template_url')) ?>',
            homeUrl: '<?= addslashes(home_url()) ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`
    </script>
    <script src="<?= get_template_directory_uri() ?>/public/index.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= KEYS['GOOGLE_MAPS_API_KEY'] ?>"></script>
    <noscript>
        Por favor ative o Javascript do seu navegador para poder experienciar melhor o site
    </noscript>
    <?php wp_footer() ?>
</body>
</html>

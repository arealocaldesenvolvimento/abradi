<?php

get_header();

$location = get_field('localizacao_contato', $_SESSION['regional']);
$address = get_field('endereco_contato', $_SESSION['regional']);

$regionalContent = !empty(get_field('conteudo_contato', $_SESSION['regional']))
    ? get_field('conteudo_contato', $_SESSION['regional'])
    : get_field('conteudo_contato', GLOBALS['ID_NACIONAL']);

$regionalEmail = !empty(get_field('email_contato', $_SESSION['regional']))
    ? get_field('email_contato', $_SESSION['regional'])
    : get_field('email_contato', GLOBALS['ID_NACIONAL']);

$regionalPhone = !empty(get_field('telefone_contato', $_SESSION['regional']))
    ? get_field('telefone_contato', $_SESSION['regional'])
    : get_field('telefone_contato', GLOBALS['ID_NACIONAL']);

?>

    <section class="section-contato">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <h1 class="section-title-01">Contato</h1>
        </div>
        <div class="row-form">
            <div class="row-form-left">
                <img
                    src="<?= get_image_url('contato/contato-banner.png') ?>"
                    alt="Mulher loira com um iphone no ouvido falando"
                    title="Contato"
                />
                <div class="content">
                    <p><?= $regionalContent ?></p>
                    <div class="contacts">
                        <p>
                            <img
                                src="<?= get_image_url('contato/fone-de-ouvido.svg') ?>"
                                alt="Fone de ouvido azul"
                            />
                            <?= $regionalPhone ?>
                        </p>
                        <p>
                            <img
                                src="<?= get_image_url('contato/envelope.svg') ?>"
                                alt="Envelope azul"
                            />
                            <?= $regionalEmail ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-form-right">
                <div class="form-container">
                    <input type="hidden" id="email-regional" name="email" value="<?= $regionalEmail ?>">
                    <?= do_shortcode('[contact-form-7 id="580" title="Contato" html_id="form-contato"]') ?>
                </div>
            </div>
        </div>
        <div class="al-small-container">
            <div class="localizacao">
                <h2 class="section-title-01">Localização</h2>
                <?php if ($location && $address): ?>
                    <div class="infos">
                        <p>
                            <img
                                src="<?= get_image_url('single-eventos/maps-azul.svg') ?>"
                                alt="Localização"
                                title="Localização"
                            />
                            <?= $address ?>
                        </p>
                        <a
                            href="http://maps.google.com/?q=<?= $address ?>"
                            title="Como Chegar?"
                            target="_blank"
                        >
                            Como Chegar?
                        </a>
                    </div>
                    <div class="mapa">
                        <input class="lat" type="hidden" value="<?= $location['lat'] ?>">
                        <input class="lng" type="hidden" value="<?= $location['lng'] ?>">
                        <section id="cd-google-map">
                            <div id="google-container"></div>
                            <div id="cd-zoom-in"></div>
                            <div id="cd-zoom-out"></div>
                        </section>
                    </div>
                <?php else: ?>
                    <div class="infos">
                        <p>
                            <img
                                src="<?= get_image_url('single-eventos/maps-azul.svg') ?>"
                                alt="Localização"
                                title="Localização"
                            />
                            Rua Oscar Freire, nº 2379 - Pinheiros, São Paulo/SP 05409-012
                        </p>
                        <a
                            href="https://goo.gl/maps/37UovKDuPXrRu7Pg9"
                            title="Como Chegar?"
                            target="_blank"
                        >
                            Como Chegar?
                        </a>
                    </div>
                    <div class="mapa">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.445949088516!2d-46.680093485022404!3d-23.552421984687385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce578fa0370cc3%3A0x2af59db66a7bb4e9!2sRua%20Oscar%20Freire%2C%202379%20-%20Pinheiros%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2005409-120!5e0!3m2!1spt-BR!2sbr!4v1583874066176!5m2!1spt-BR!2sbr"
                            frameborder="0"
                            allowfullscreen=""
                            aria-hidden="false"
                        >
                        </iframe>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/imask.js"></script>
    <script src="<?= get_template_directory_uri() ?>/public/page-contato.js" defer></script>

<?php get_footer() ?>


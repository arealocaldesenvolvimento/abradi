<?php

get_header();

$regionalContent = !empty(get_field('sobre', $_SESSION['regional']))
    ? get_field('sobre', $_SESSION['regional'])
    : get_field('sobre', GLOBALS['ID_NACIONAL']);

?>

    <section class="section-a-associacao">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <?php while (have_posts()): the_post() ?>
                <?php if ($regionalContent): ?>
                    <h1 class="section-title-01"><?= get_the_title() ?></h1>
                    <p><?= $regionalContent ?></p>
                <?php endif ?>
            <?php endwhile ?>
            <div class="estatuto-e-regimento">
                <h2 class="section-title-01">Estatuto e Regimento</h2>
                <?php $statute = pageEstatutoERegimentoQuery() ?>
                <?php if ($statute->have_posts()): ?>
                    <div>
                        <?php while ($statute->have_posts()): $statute->the_post() ?>
                            <article>
                                <?= setPostThumbnail() ?>
                                <div class="content">
                                    <h3><?= get_the_title() ?></h3>
                                    <a
                                        href="<?= get_field('arquivo')['url'] ?>"
                                        target="_blank"
                                        download
                                    >
                                        Faça o download
                                        <img
                                            src="<?= get_image_url('a-associacao/flecha-download.svg') ?>"
                                            alt="Download"
                                        />
                                    </a>
                                </div>
                            </article>
                        <?php endwhile ?>
                    </div>
                <?php else: ?>
                    <p class="notfound-message">Não foram encontradas estatutos e regimentos</p>
                <?php endif ?>
            </div>
        </div>
    </section>

<?php get_footer() ?>

<?php

get_header();

$management = get_queried_object()->slug;
$regionalSlug = get_post($_SESSION['regional'])->post_name;

$managements = sortManagements($regionalSlug);

$directorsship = sortDirectorsship($regionalSlug, $management);
$directors = pageDiretoriaQueries($regionalSlug, $management);

?>

    <section class="section-diretoria">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <?php if (empty($managements)): ?>
                <h1 class="section-title-01">
                    Não foram cadastrados diretores para <span><?= get_post($_SESSION['regional'])->post_title ?></span>
                </h1>
            <?php else: ?>
                <h1 class="section-title-01">Presidência</h1>
            <?php endif ?>
            <div>
                <nav class="gestoes">
                    <ul>
                        <?php foreach ($managements as $manag): ?>
                            <li>
                                <a
                                    class="<?= isActive($manag['slug']) ?>"
                                    href="<?= get_term_link($manag['slug'], 'gestoes') ?>"
                                >
                                    Gestão <?= $manag['name'] ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </nav>
                <div class="diretores">
                    <?php if (!empty($directorsship)): ?>
                        <section>
                            <h2 class="section-title-01">Diretoria</h2>
                            <div>
                                <?php foreach ($directorsship as $director): ?>
                                    <article>
                                        <div><?= $director['thumb'] ?></div>
                                        <div class="content">
                                            <p class="cargo"><?= $director['role'] ?></p>
                                            <h3><?= $director['name'] ?></h3>
                                            <p class="empresa"><?= $director['company'] ?></p>
                                            <p><?= $director['degree'] ?></p>
                                        </div>
                                    </article>
                                <?php endforeach ?>
                            </div>
                        </section>
                    <?php endif ?>
                    <?php if ($directors->directors->have_posts()): ?>
                        <section>
                            <h2 class="section-title-01">Diretores</h2>
                            <div>
                                <?php while ($directors->directors->have_posts()): $directors->directors->the_post() ?>
                                    <article>
                                        <div>
                                            <?= setPostThumbnail() ?>
                                        </div>
                                        <div class="content">
                                            <p class="cargo"><?= get_field('cargo') ?></p>
                                            <h3><?= get_the_title() ?></h3>
                                            <p class="empresa"><?= get_field('empresa') ?></p>
                                            <p><?= get_field('formacoes') ?></p>
                                        </div>
                                    </article>
                                <?php endwhile ?>
                            </div>
                        </section>
                    <?php endif ?>
                    <?php if ($directors->executives->have_posts()): ?>
                        <section>
                            <h2 class="section-title-01">Corpo Executivo</h2>
                            <div>
                                <?php while ($directors->executives->have_posts()): $directors->executives->the_post() ?>
                                    <article>
                                        <div>
                                            <?= setPostThumbnail() ?>
                                        </div>
                                        <div class="content">
                                            <p class="cargo"><?= get_field('cargo') ?></p>
                                            <h3><?= get_the_title() ?></h3>
                                            <p class="empresa"><?= get_field('empresa') ?></p>
                                            <p><?= get_field('formacoes') ?></p>
                                        </div>
                                    </article>
                                <?php endwhile ?>
                            </div>
                        </section>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer() ?>

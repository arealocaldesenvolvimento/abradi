/**
 * Modules
 */
const mix = require('laravel-mix')

/**
 * SCSS
 */
mix
	.sass('assets/scss/404.scss', 'public/404.css')
	.sass(
		'assets/scss/archive-colaboradores.scss',
		'public/archive-colaboradores.css'
	)
	.sass('assets/scss/archive-comites.scss', 'public/archive-comites.css')
	.sass('assets/scss/archive-regionais.scss', 'public/archive-regionais.css')
	.sass('assets/scss/author.scss', 'public/author.css')
	.sass('assets/scss/page-a-associacao.scss', 'public/page-a-associacao.css')
	.sass(
		'assets/scss/page-agentes-digitais.scss',
		'public/page-agentes-digitais.css'
	)
	.sass('assets/scss/page-associe-se.scss', 'public/page-associe-se.css')
	.sass('assets/scss/page-contato.scss', 'public/page-contato.css')
	.sass(
		'assets/scss/page-convenios-e-beneficios.scss',
		'public/page-convenios-e-beneficios.css'
	)
	.sass('assets/scss/page-diretoria.scss', 'public/page-diretoria.css')
	.sass(
		'assets/scss/page-estatuto-e-regimento.scss',
		'public/page-estatuto-e-regimento.css'
	)
	.sass('assets/scss/archive-agenda.scss', 'public/archive-agenda.css')
	.sass('assets/scss/page-materiais.scss', 'public/page-materiais.css')
	.sass('assets/scss/page-parceiros.scss', 'public/page-parceiros.css')
	.sass('assets/scss/page-noticias.scss', 'public/page-noticias.css')
	.sass('assets/scss/search.scss', 'public/search.css')
	.sass('assets/scss/single.scss', 'public/single.css')
	.sass('assets/scss/single-agenda.scss', 'public/single-agenda.css')
	.sass('assets/scss/single-comites.scss', 'public/single-comites.css')
	.sass('assets/scss/single-regionais.scss', 'public/single-regionais.css')

/**
 * JS Files to compile
 */
mix
// .scripts('assets/js/libs/tiny-slider.js', 'public/tiny-slider.js')
// .scripts('assets/js/libs/imask.js', 'public/imask.js')
// .scripts('assets/js/libs/sweetalert2@9.js', 'public/sweetalert2@9.js')
// .scripts(
// 	'assets/js/archive-colaboradores.js',
// 	'public/archive-colaboradores.js'
// )
// .scripts('assets/js/author.js', 'public/author.js')
// .scripts('assets/js/index.js', 'public/index.js')
// .scripts('assets/js/page-associe-se.js', 'public/page-associe-se.js')
// .scripts('assets/js/page-contato.js', 'public/page-contato.js')
// .scripts(
// 	'assets/js/page-convenios-e-beneficios.js',
// 	'public/page-convenios-e-beneficios.js'
// )
// .scripts('assets/js/archive-agenda.js', 'public/archive-agenda.js')
// .scripts('assets/js/single.js', 'public/single.js')
// .scripts('assets/js/single-agenda.js', 'public/single-agenda.js')
// .scripts('assets/js/single-comites.js', 'public/single-comites.js')
// .scripts('assets/js/single-convenio.js', 'public/single-convenio.js')
// .scripts('assets/js/single-regionais.js', 'public/single-regionais.js')
// .scripts(
// 	'assets/js/single-regionais-newsletter.js',
// 	'public/single-regionais-newsletter.js'
// )
// .scripts('assets/js/google-recaptcha.js', 'public/google-recaptcha.js')

/**
 * JS Files to watch
 */
mix
// .js('assets/js/archive-colaboradores.js', 'public/archive-colaboradores.js')
// .js('assets/js/author.js', 'public/author.js')
// .js('assets/js/index.js', 'public/index.js')
// .js('assets/js/page-associe-se.js', 'public/page-associe-se.js')
// .js('assets/js/page-contato.js', 'public/page-contato.js')
// .js(
// 	'assets/js/page-convenios-e-beneficios.js',
// 	'public/page-convenios-e-beneficios.js'
// )
// .js('assets/js/archive-agenda.js', 'public/archive-agenda.js')
// .js('assets/js/single.js', 'public/single.js')
// .js('assets/js/single-agenda.js', 'public/single-agenda.js')
// .js('assets/js/single-comites.js', 'public/single-comites.js')
// .js('assets/js/single-convenio.js', 'public/single-convenio.js')
// .js('assets/js/single-regionais.js', 'public/single-regionais.js')
// .js(
// 	'assets/js/single-regionais-newsletter.js',
// 	'public/single-regionais-newsletter.js'
// )
// .js('assets/js/google-recaptcha.js', 'public/google-recaptcha.js')

/**
 * Configs
 */
mix
	.setPublicPath('public')
	.options({ processCssUrls: false })
	.sourceMaps()

/**
 * Production
 */
if (mix.inProduction()) {
	mix.version()
}

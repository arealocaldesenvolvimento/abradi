<?php

get_header();

$userSearchQuery = $_GET['nome'] ?? '';
$regionalUf = get_field('estado', $_SESSION['regional']);

$states = GLOBALS['STATES'];
unset($states['Nacional'], $states['Norte']);

?>
    <section class="section-agentes-digitais">
        <div class="al-small-container">
            <nav class="breadcrumb">
                    <?= yoast_breadcrumb() ?>
            </nav>
            <h1 class="section-title-01">Agentes Digitais</h1>
            <div class="agentes-digitais-search-bar">
                <?php
                    $areasOfExpertise = new WP_Query([
                        'post_type' => 'area-atuacao',
                        'posts_per_page' => -1,
                        'orderby' => 'date',
                        'order' => 'DESC',
                    ]);
                ?>
                <form
                    action="<?= home_url('/agentes-digitais') ?>"
                    method="GET"
                    role="search"
                >
                    <select name="segmento">
                        <option disabled selected>SEGMENTO</option>
                        <option value="">Todos</option>
                        <?php while ($areasOfExpertise->have_posts()): $areasOfExpertise->the_post() ?>
                            <?php $id = get_the_ID() ?>
                            <option
                                value="<?= $id ?>"
                                <?= $id == $_GET['segmento'] ? 'selected' : '' ?>
                            >
                                <?= get_the_title() ?>
                            </option>
                        <?php endwhile ?>
                    </select>
                    <select name="estado">
                        <option value="todos">Todos</option>
                        <?php foreach ($states as $state): ?>
                            <option
                                value="<?= $state ?>"
                                <?php if (!empty($_GET['estado'])): ?>
                                    <?= $state === $_GET['estado'] ? 'selected' : '' ?>
                                <?php else: ?>
                                    <?= $state === $regionalUf ? 'selected' : '' ?>
                                <?php endif ?>
                            >
                                <?= $state ?>
                            </option>
                        <?php endforeach ?>
                    </select>
                    <input
                        type="text"
                        name="nome"
                        value="<?= $userSearchQuery ?>"
                        placeholder="Digite..."
                    >
                    <button type="submit">Pesquisar</button>
                </form>
            </div>
            <div class="agentes-digitais">
                <?php $users = pageAgentesDigitaisQuery($userSearchQuery, $regionalUf) ?>
                <?php if ($users->results->get_results()): ?>
                    <?php foreach ($users->results->get_results() as $user): ?>
                        <?php
                            $title = get_user_meta($user->ID, 'empresa_membro', true) ?? $user->display_name;
                            $state = get_user_meta($user->ID, 'estado_membro', true);
                            $city = get_user_meta($user->ID, 'cidade_membro', true);
                            $company = get_user_meta($colaborador->ID, 'empresa_membro', true);
                            $url = esc_url(get_author_posts_url($user->ID, $user->user_nicename));
                            $logo = get_user_meta($user->ID, 'logomarca_membro', true)
                        ?>
                        <article>
                            <a href="<?= $url ?>" title="<?= $company ?>">
                                <?php if ($logo): ?>
                                    <?php if (is_array($logo)): ?>
                                        <?= wp_get_attachment_image($logo['ID'], 'medium') ?>
                                    <?php elseif (wp_get_attachment_image($logo, 'medium')): ?>
                                        <?= wp_get_attachment_image($logo, 'medium') ?>
                                    <?php else: ?>
                                        <img
                                            src="<?= get_image_url('default-thumbnail.jpg') ?>"
                                            alt="Imagem destacada padrão para postagens"
                                        >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img
                                        src="<?= get_image_url('default-thumbnail.jpg') ?>"
                                        alt="Imagem destacada padrão para postagens"
                                    >
                                <?php endif ?>
                            </a>
                            <div class="content">
                                <a href="<?= $url ?>" title="<?= $company ?>">
                                    <h2><?= wp_trim_words($title, 4) ?></h2>
                                    <p><?= $city ? $city . ' - ' . $state : $state ?></p>
                                </a>
                            </div>
                        </article>
                    <?php endforeach ?>
                <?php else: ?>
                    <p class="notfound-message">
                        Não foram encontrados agentes digitais para:
                        <?php foreach($users->searchValuesToDisplay as $value): ?>
                            <span class="notfound-filter"><?= $value . ' ' ?></span>
                        <?php endforeach ?>
                    </p>
                <?php endif ?>
            </div>
            <div class="pagination">
                <?= paginationLinks($users->results, $users->perPage) ?>
            </div>
        </div>
    </section>

<?php get_footer() ?>


<?php get_header() ?>

    <section class="section-estatuto-e-regimento">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <div class="estatuto-e-regimento">
                <h1 class="section-title-01">Estatuto e Regimento</h1>
                <?php $statutesAndRegiments = pageEstatutoERegimentoQuery() ?>
                <?php if ($statutesAndRegiments->have_posts()): ?>
                    <div>
                        <?php while ($statutesAndRegiments->have_posts()): $statutesAndRegiments->the_post() ?>
                            <article>
                                <?= setPostThumbnail() ?>
                                <div class="content">
                                    <h2><?= get_the_title() ?></h2>
                                    <a
                                        href="<?= get_field('arquivo')['url'] ?>"
                                        target="_blank"
                                        download
                                    >
                                        Faça o download
                                        <img
                                            src="<?= get_image_url('a-associacao/flecha-download.svg') ?>"
                                            alt="Download"
                                        />
                                    </a>
                                </div>
                            </article>
                        <?php endwhile ?>
                    </div>
                <?php else: ?>
                    <p class="notfound-message">Não foram encontradas notícias</p>
                <?php endif ?>
            </div>
        </div>
    </section>

<?php get_footer() ?>

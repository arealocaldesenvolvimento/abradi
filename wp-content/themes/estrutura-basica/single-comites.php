<?php

get_header();

$regionalSlug = get_post($_SESSION['regional'])->post_name;

?>

    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css"
        rel="stylesheet"
    >
    <section class="section-single-comites">
        <div class="al-small-container">
            <nav class="breadcrumb">
                    <?= yoast_breadcrumb() ?>
            </nav>
            <div class="comite">
                <?php while (have_posts()): the_post() ?>
                    <h1 class="section-title-01"><?= get_the_title() ?></h1>
                    <?= setPostThumbnail() ?>
                    <div class="content">
                        <p><?= wpautop(sanitizeString(get_the_content())) ?></p>
                    </div>
                <?php endwhile ?>
            </div>
            <?php if (have_rows('membros_comite')): ?>
                <section class="membros-comite">
                    <h2 class="section-title-01">Membros</h2>
                    <div>
                        <?php while (have_rows('membros_comite')): the_row() ?>
                            <?php $name = get_sub_field('nome') ?>
                            <article>
                                <div>
                                    <img
                                        src="<?= get_sub_field('foto')['sizes']['medium'] ?? get_image_url('default-thumbnail.jpg') ?>"
                                        alt="<?= $name ?>"
                                    />
                                </div>
                                <div class="content">
                                    <h3><?= $name ?></h3>
                                    <p class="empresa"><?= get_sub_field('empresa') ?></p>
                                    <p><?= get_sub_field('formacoes') ?></p>
                                </div>
                            </article>
                        <?php endwhile ?>
                    </div>
                </section>
            <?php endif ?>
            <?php if (!empty($dest = get_field('destinatario'))): ?>
                <div class="contato">
                    <h2 class="section-title-01">Entre em Contato</h2>
                    <section class="form-container">
                        <?= do_shortcode('[contact-form-7 id="10826" title="Comitê"]') ?>
                        <input
                            type="hidden"
                            name="comite-hidden"
                            value="<?= get_the_title() ?>"
                        >
                        <input
                            type="hidden"
                            name="destinatario-hidden"
                            value="<?= $dest ?>"
                        >
                    </section>
                </div>
            <?php endif ?>
            <div class="slider-single-comites-container">
                <?php $relatedNews = singleComitesQuery($regionalSlug) ?>
                <?php if ($relatedNews->have_posts()):  ?>
                    <h2 class="section-title-01">Fique por dentro!</h2>
                    <div id="slider-single-comites">
                        <?php while ($relatedNews->have_posts()): $relatedNews->the_post() ?>
                            <?php $permalink = get_permalink() ?>
                            <div class="item">
                                <a href="<?= $permalink ?>">
                                    <?= setPostThumbnail() ?>
                                </a>
                                <div class="content">
                                    <a href="<?= $permalink ?>">
                                        <h3>
                                            <?= wp_trim_words(get_the_title(), 8) ?>
                                        </h3>
                                    </a>
                                    <a href="<?= $permalink ?>">
                                        <p><?= sanitizeString(get_the_excerpt()) ?></p>
                                    </a>
                                    <a href="<?= $permalink ?>">
                                        <span><?= get_the_date() ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/tiny-slider.js"></script>
    <script src="<?= get_template_directory_uri() ?>/public/imask.js"></script>
    <script src="<?= get_template_directory_uri() ?>/public/single-comites.js" defer></script>

<?php get_footer() ?>


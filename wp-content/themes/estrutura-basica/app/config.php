<?php

/**
 * API settings
 */
define('KEYS', [
    'CLICKSIGN_API_TOKEN' => '748d4729-d31a-4908-8abc-430bc9cdbccb',
    'CLICKSIGN_API_URL' => 'https://app.clicksign.com/api/v1/documents',

    'CLICKSIGN_API_TOKEN_SANDBOX' => '54659cfb-79ab-4c26-b4a7-3d119a08fa1b',
    'CLICKSIGN_API_URL_SANDBOX' => 'https://sandbox.clicksign.com/api/v1/documents',

    'RDSTATION_API_TOKEN' => '07dfddde92698d700c2891a87791107b',

    'GOOGLE_MAPS_API_KEY' => 'AIzaSyAAD3pu9fqjmbafITcqctlEXXIyzki-JLI',
    'GOOGLE_CAPTCHA_HOME_KEY' => '6LcSn_UUAAAAAG_WMRkkIVTHkjogVxPC-GhcouG4'
]);

/**
 * Global theme variables
 */
define('GLOBALS', [
    'ID_NACIONAL' => 352,
    'THUMB_SIZE' => 'medium',
    'STATES' => [
        'Acre' => 'AC',
        'Alagoas' => 'AL',
        'Amapá' => 'AP',
        'Amazonas' => 'AM',
        'Bahia' => 'BA',
        'Ceará' => 'CE',
        'Distrito Federal' => 'DF',
        'Espírito Santo' => 'ES',
        'Goiás' => 'GO',
        'Maranhão' => 'MA',
        'Mato Grosso' => 'MT',
        'Mato Grosso do Sul' => 'MS',
        'Minas Gerais' => 'MG',
        'Nacional' => 'Nacional',
        'Norte' => 'Norte',
        'Pará' => 'PA',
        'Paraíba' => 'PB',
        'Paraná' => 'PR',
        'Pernambuco' => 'PE',
        'Piauí' => 'PI',
        'Rio de Janeiro' => 'RJ',
        'Rio Grande do Norte' => 'RN',
        'Rio Grande do Sul' => 'RS',
        'Rondônia' => 'RO',
        'Roraima' => 'RR',
        'Santa Catarina' => 'SC',
        'São Paulo' => 'SP',
        'Sergipe' => 'SE',
        'Tocantins' => 'TO',
    ],
    'DIRECTORS' => [
        'abradiPresident' => [
            'email' => 'marcos.moraes@rpmacomunicacao.com.br',
            'name' => 'Marcos Moraes',
            'message'=> 'Olá Marcos Moraes. Um novo termo de adesão foi enviado.'
        ],
        'responsible' => [
            'email' => 'paulo.centenaro@abradi.com.br',
            'name' => 'Paulo Centenaro Filho',
            'message'=> 'Olá Paulo. Um novo termo de adesão foi enviado.'
        ],
    ],
]);

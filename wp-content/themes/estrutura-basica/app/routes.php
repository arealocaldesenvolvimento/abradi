<?php

namespace App\Routes;

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';
include_once get_theme_file_path('app/controllers/CityController.php');
include_once get_theme_file_path('app/controllers/StateController.php');
include_once get_theme_file_path('app/controllers/MemberController.php');
include_once get_theme_file_path('app/controllers/RegionalController.php');

class Routes
{
    /**
     * Custom REST routes
     *
     * @return void
     */
    public static function routes(): void
    {
        $namespace = 'api';

        /**
         * GET wp-json/api/estados
         */
        register_rest_route($namespace, '/estados', [
            'methods' => 'GET',
            'callback' => 'getStates'
        ]);

        /**
         * POST wp-json/api/cidades
         */
        register_rest_route($namespace, '/cidades', [
            'methods' => 'POST',
            'callback' => 'getCities'
        ]);

        /**
         * POST wp-json/api/associe-se
         */
        register_rest_route($namespace, '/associe-se', [
            'methods' => 'POST',
            'callback' => 'storeMember'
        ]);

        /**
         * GET wp-json/api/regionais
         */
        register_rest_route($namespace, '/regionais', [
            'methods' => 'GET',
            'callback' => 'getRegionais'
        ]);
    }
}

Routes::routes();

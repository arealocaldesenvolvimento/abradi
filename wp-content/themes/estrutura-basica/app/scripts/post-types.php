<?php

flush_rewrite_rules();

/**
 * Regionais
 */
function regionais(): void
{
    register_post_type('regionais', [
        'labels' => [
            'name' => _x('Regionais', 'post type general name'),
            'singular_name' => _x('Regional', 'post type singular name'),
            'add_new' => __('Adicionar nova regional'),
            'add_new_item' => __('Adicionar nova regional'),
            'edit_item' => __('Editar regional'),
            'new_item' => __('Nova regional'),
            'view_item' => __('Ver regional'),
            'not_found' =>  __('Regionais não encontradas'),
            'not_found_in_trash' => __('Nenhuma regional foi encontrada na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Regionais'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-admin-site',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'custom-fields', 'revisions', 'author']
    ]);
}
add_action('init', 'regionais');

/**
 * Projetos
 */
function projetos(): void
{
    register_post_type('projetos', [
        'labels' => [
            'name' => _x('Materiais', 'post type general name'),
            'singular_name' => _x('Material', 'post type singular name'),
            'add_new' => __('Adicionar novo material'),
            'add_new_item' => __('Adicionar novo material'),
            'edit_item' => __('Editar material'),
            'new_item' => __('Novo material'),
            'view_item' => __('Ver material'),
            'not_found' =>  __('Materiais não encontrados'),
            'not_found_in_trash' => __('Nenhum material foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Materiais'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-media-document',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'editor', 'thumbnail', 'custom-fields', 'revisions']
    ]);
}
add_action('init', 'projetos');

/**
 * Materiais parceiros
 */
function materiais_parceiros(): void
{
    register_post_type('materiais-parceiros', [
        'labels' => [
            'name' => _x('Materiais parceiros', 'post type general name'),
            'singular_name' => _x('Material', 'post type singular name'),
            'add_new' => __('Adicionar novo material'),
            'add_new_item' => __('Adicionar novo material'),
            'edit_item' => __('Editar material'),
            'new_item' => __('Novo material'),
            'view_item' => __('Ver material'),
            'not_found' =>  __('Materiais não encontrados'),
            'not_found_in_trash' => __('Nenhum material foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Materiais parceiros'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-media-document',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'editor', 'thumbnail', 'custom-fields', 'revisions']
    ]);
}
add_action('init', 'materiais_parceiros');

/**
 * Agendas
 */
function agendas(): void
{
    register_post_type('agenda', [
        'label' => 'agenda',
        'labels' => [
            'name' => _x('Eventos', 'post type general name'),
            'singular_name' => _x('Evento', 'post type singular name'),
            'add_new' => __('Adicionar novo evento'),
            'add_new_item' => __('Adicionar novo evento'),
            'edit_item' => __('Editar evento'),
            'new_item' => __('Novo evento'),
            'view_item' => __('Ver evento'),
            'not_found' =>  __('Eventos não encontrados'),
            'not_found_in_trash' => __('Nenhum evento foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Eventos'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-calendar-alt',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'custom-fields', 'thumbnail', 'editor', 'revisions']
    ]);
}
add_action('init', 'agendas');

/**
 * Convênios
 */
function convenio(): void
{
    register_post_type('convenio', [
        'labels' => [
            'name' => _x('Benefícios', 'post type general name'),
            'singular_name' => _x('Benefício', 'post type singular name'),
            'add_new' => __('Adicionar novo benefício'),
            'add_new_item' => __('Adicionar novo benefício'),
            'edit_item' => __('Editar benefício'),
            'new_item' => __('Novo benefício'),
            'view_item' => __('Ver benefício'),
            'not_found' =>  __('Benefícios não encontrados'),
            'not_found_in_trash' => __('Nenhum benefício foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Benefícios'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-awards',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'editor', 'custom-fields', 'revisions']
    ]);
}
add_action('init', 'convenio');

/**
 * Registra a taxonomia "categoria_convenio", apelidada de "Segmentos/Segmento"
 */
register_taxonomy(
    'categoria_convenio',
    'convenio',
    [
        'label' => 'Segmentos',
        'singular_label' => 'Segmento',
        'rewrite' => true,
        'hierarchical' => true,
    ]
);

/**
 * Diretores
 */
function diretores(): void
{
    register_post_type('diretores', [
        'labels' => [
            'name' => _x('Diretores', 'post type general name'),
            'singular_name' => _x('Diretor', 'post type singular name'),
            'add_new' => __('Adicionar novo diretor'),
            'add_new_item' => __('Adicionar novo diretor'),
            'edit_item' => __('Editar diretor'),
            'new_item' => __('Novo diretor'),
            'view_item' => __('Ver diretor'),
            'not_found' =>  __('Diretores não encontrados'),
            'not_found_in_trash' => __('Nenhum diretor foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Diretores'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-businessperson',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'custom-fields', 'revisions']
    ]);
}
add_action('init', 'diretores');

/**
 * Registra a taxonomia "regionais"
 */
register_taxonomy(
    'regionais',
    [
        'post',
        'agenda',
        'diretores',
        'colaboradores',
        'projetos',
    ],
    [
        'label' => 'Regionais',
        'singular_label' => 'Regional',
        'rewrite' => true,
        'hierarchical' => true,
    ]
);

/**
 * Registra a taxonomia "catAgenda"
 */
register_taxonomy(
    'catAgenda',
    'agenda',
    [
        'label' => 'Categorias',
        'singular_label' => 'Categoria',
        'rewrite' => true,
        'hierarchical' => true
    ]
);

/**
 * Registra a taxonomia "gestoes"
 */
register_taxonomy(
    'gestoes',
    'diretores',
    [
        'label' => 'Gestões',
        'singular_label' => 'Gestão',
        'rewrite' => true,
        'hierarchical' => true,
    ]
);

/**
 * Registra a taxonomia "cargos"
 */
register_taxonomy(
    'cargos',
    'diretores',
    [
        'label' => 'Cargos',
        'singular_label' => 'Cargo',
        'rewrite' => true,
        'hierarchical' => true,
    ]
);

/**
 * Estatutos e Regimentos
 */
function estatutosRegimentos(): void
{
    register_post_type('estatutos-regimentos', [
        'labels' => [
            'name' => _x('Estatutos e Regimentos', 'post type general name'),
            'singular_name' => _x('Estatuto e Regimento', 'post type singular name'),
            'add_new' => __('Adicionar novo estatuto e regimento'),
            'add_new_item' => __('Adicionar novo estatuto e regimento'),
            'edit_item' => __('Editar estatuto e regimento'),
            'new_item' => __('Novo estatuto e regimento'),
            'view_item' => __('Ver estatuto e regimento'),
            'not_found' =>  __('Nenhum estatuto e regimento encontrado'),
            'not_found_in_trash' => __('Nenhum estatuto e regimento foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Estatutos e Regimentos'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-book',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'custom-fields', 'revisions']
    ]);
}
add_action('init', 'estatutosRegimentos');

/**
 * Colaboradores
 */
function colaboradores(): void
{
    register_post_type('colaboradores', [
        'labels' => [
            'name' => _x('Colaboradores', 'post type general name'),
            'singular_name' => _x('Colaborador', 'post type singular name'),
            'add_new' => __('Adicionar novo colaborador'),
            'add_new_item' => __('Adicionar novo colaborador'),
            'edit_item' => __('Editar colaborador'),
            'new_item' => __('Novo colaborador'),
            'view_item' => __('Ver colaborador'),
            'not_found' =>  __('Colaboradores não encontrados'),
            'not_found_in_trash' => __('Nenhum colaborador foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Colaboradores'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-thumbs-up',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'custom-fields', 'revisions']
    ]);
}
add_action('init', 'colaboradores');

/**
 * Comitês
 */
function comites(): void
{
    register_post_type('comites', [
        'labels' => [
            'name' => _x('Comitês', 'post type general name'),
            'singular_name' => _x('Comitê', 'post type singular name'),
            'add_new' => __('Adicionar novo comitê'),
            'add_new_item' => __('Adicionar novo comitê'),
            'edit_item' => __('Editar comitê'),
            'new_item' => __('Novo comitê'),
            'view_item' => __('Ver comitê'),
            'not_found' =>  __('Comitês não encontrados'),
            'not_found_in_trash' => __('Nenhum comitê foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Comitês'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-groups',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'editor', 'custom-fields', 'revisions']
    ]);
}
add_action('init', 'comites');

/**
 * Áreas de Atuação
 */
function areasDeAtuacao(): void
{
    register_post_type('area-atuacao', [
        'label' => 'area-atuacao',
        'labels' => [
            'name' => _x('Áreas de Atuação', 'post type general name'),
            'singular_name' => _x('Área de Atuação', 'post type singular name'),
            'add_new' => __('Adicionar nova área de atuação'),
            'add_new_item' => __('Adicionar nova área de atuação'),
            'edit_item' => __('Editar área de atuação'),
            'new_item' => __('Nova área de atuação'),
            'view_item' => __('Ver área de atuação'),
            'not_found' =>  __('Áreas de Atuação não encontradas'),
            'not_found_in_trash' => __('Nenhuma área de atuação foi encontrada na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Áreas de Atuação'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'revisions']
    ]);
}
add_action('init', 'areasDeAtuacao');

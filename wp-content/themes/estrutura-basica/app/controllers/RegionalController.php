<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Fetch all regionais
 *
 * @return WP_REST_Response
 */
function getRegionais(): WP_REST_Response
{
    $regionaisPosts = new WP_Query([
        'post_type' => 'regionais',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
    ]);

    $regionais = [];
    while ($regionaisPosts->have_posts()) {
        $regionaisPosts->the_post();

        $regionais[] = [
            'name' => get_the_title(),
            'email' => get_field('email_presidente'),
            'state' => get_field('estado')
        ];
    }

    if (!empty($regionais)) {
        return new WP_REST_Response([
            'status' => 200,
            'regionais' => $regionais,
            'message' => 'Regionais obtidas com sucesso'
        ]);
    }

    return new WP_REST_Response([
        'status' => 500,
        'message' => 'Erro ao obter as regionais'
    ]);
}

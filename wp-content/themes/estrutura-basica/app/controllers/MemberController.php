<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Register a new member ("join" form), send the membership term to clicksign
 * and register a new lead at RD Station
 *
 * @param WP_REST_Request $request
 * @return WP_REST_Response
 */
function storeMember(WP_REST_Request $request): WP_REST_Response
{
    $memberData = $request->get_json_params();

    if (in_array('', $memberData, true) || in_array(null, $memberData, true))
        return new WP_REST_Response([
            'status' => 400,
            'message' => 'Por favor, preencha todos os campos obrigatórios'
        ]);

    try {
        if (!get_user_by('login', $memberData['nomeFantasia']) && !get_user_by('email', $memberData['email'])) {
            $newMember = wp_insert_user([
                'user_login' => $memberData['nomeFantasia'],
                'user_url' => $memberData['site'],
                'user_email' => $memberData['email'],
                'first_name' => $memberData['nomeFantasia'],
                'role' => 'espera'
            ]);

            update_user_meta(
                $newMember,
                'tipo_membro',
                sanitize_text_field($memberData['tipoEmpresa'])
            );
            update_user_meta(
                $newMember,
                'representanteLegal_membro',
                sanitize_text_field($memberData['representanteLegal'])
            );
            update_user_meta(
                $newMember,
                'empresa_membro',
                sanitize_text_field($memberData['empresa'])
            );
            update_user_meta(
                $newMember,
                'nomeFantasia_membro',
                sanitize_text_field($memberData['nomeFantasia'])
            );
            update_user_meta(
                $newMember,
                'cnpj_membro',
                $memberData['cnpj']);
            update_user_meta(
                $newMember,
                'endereco_membro',
                sanitize_text_field($memberData['endereco'])
            );
            update_user_meta(
                $newMember,
                'complemento_membro',
                sanitize_text_field($memberData['complemento'])
            );
            update_user_meta(
                $newMember,
                'estado_membro',
                sanitize_text_field($memberData['estado'])
            );
            update_user_meta(
                $newMember,
                'cidade_membro',
                sanitize_text_field($memberData['cidade'])
            );
            update_user_meta(
                $newMember,
                'cep_membro',
                sanitize_text_field($memberData['cep'])
            );
            update_user_meta(
                $newMember,
                'telefone_membro',
                sanitize_text_field($memberData['telefone'])
            );
            update_user_meta(
                $newMember,
                'site_membro',
                sanitize_text_field($memberData['site'])
            );
            update_user_meta(
                $newMember,
                'empresa_membro',
                sanitize_text_field($memberData['empresa'])
            );
            update_user_meta(
                $newMember,
                'cargo_membro',
                sanitize_text_field($memberData['cargo'])
            );
            update_user_meta(
                $newMember,
                'email_membro',
                sanitize_text_field($memberData['email'])
            );
            update_user_meta(
                $newMember,
                'celular_membro',
                sanitize_text_field($memberData['celular'])
            );
            update_user_meta(
                $newMember,
                'emailCobranca_membro',
                sanitize_text_field($memberData['emailCobranca'])
            );
            update_user_meta(
                $newMember,
                'mensalidade_membro',
                sanitize_text_field($memberData['mensalidade'])
            );

            $memberData += [
                'identificador' => 'associe-se',
                'created_at' => date('d/m/Y H:m:i'),
                'tags' => 'Prospectos, Associe-se, Site, ABRADi'
            ];

            $adhesionTerm = [
                'document' => [
                    'path' => '/Termos de Adesão/Termo de Adesão - ' . $memberData['empresa'] . '.pdf',
                    'content_base64' => 'data:application/pdf;base64,' . generatePdf($memberData),
                    'locale' => 'pt-BR',
                    'signers' => []
                ]
            ];

            $memberRegional = new WP_Query([
                'post_type' => 'regionais',
                'posts_per_page' => 1,
                'meta_query' => [
                    [
                        'key' => 'estado',
                        'value' =>
                            $memberData['estado'] === 'AM' ||
                            $memberData['estado'] === 'AP' ||
                            $memberData['estado'] === 'AC' ||
                            $memberData['estado'] === 'PA' ||
                            $memberData['estado'] === 'RR' ||
                            $memberData['estado'] === 'RO' ||
                            $memberData['estado'] === 'TO'
                                ? 'Norte'
                                : $memberData['estado'],
                        'compare' => 'LIKE'
                    ]
                ]
            ]);

            if ($memberRegional->have_posts()) {
                $id = $memberRegional->posts[0]->ID;

                if (!empty(get_field('presidente', $id)) && get_field('termo_presidente', $id))
                    $adhesionTerm['document']['signers'][] = [
                        'email' => get_field('email_presidente', $id),
                        'sign_as' => 'sign',
                        'auths' => ['email'],
                        'name' => get_field('presidente', $id),
                        'has_documentation' => false,
                        'send_email' => true,
                        'message' => 'Olá ' . get_field('presidente', $id) . '. Por favor, assine o documento.'
                    ];

                if (get_field('termo_responsavel', $id))
                    $adhesionTerm['document']['signers'][] = [
                        'email' => GLOBALS['DIRECTORS']['responsible']['email'],
                        'sign_as' => 'sign',
                        'auths' => ['email'],
                        'name' => GLOBALS['DIRECTORS']['responsible']['name'],
                        'has_documentation' => false,
                        'send_email' => true,
                        'message' => GLOBALS['DIRECTORS']['responsible']['message']
                    ];
            } else {
                $adhesionTerm['document']['signers'][] = [
                    'email' => GLOBALS['DIRECTORS']['responsible']['email'],
                    'sign_as' => 'sign',
                    'auths' => ['email'],
                    'name' => GLOBALS['DIRECTORS']['responsible']['name'],
                    'has_documentation' => false,
                    'send_email' => true,
                    'message' => GLOBALS['DIRECTORS']['responsible']['message']
                ];
            }

            $adhesionTerm['document']['signers'][] = [
                'email' => GLOBALS['DIRECTORS']['abradiPresident']['email'],
                'sign_as' => 'sign',
                'auths' => ['email'],
                'name' => GLOBALS['DIRECTORS']['abradiPresident']['name'],
                'has_documentation' => false,
                'send_email' => true,
                'message' => GLOBALS['DIRECTORS']['abradiPresident']['message']
            ];

            $adhesionTerm['document']['signers'][] = [
                'email' => $memberData['email'],
                'sign_as' => 'sign',
                'auths' => ['email'],
                'name' => $memberData['representanteLegal'],
                'has_documentation' => false,
                'send_email' => true,
                'message' => "Olá {$memberData['representanteLegal']},\n\nMuito prazer. Sou o Leandro da ABRADi e vou lhe auxiliar com a sua associação na ABRADi.\n\nPreparamos um Termo de Adesão da associação da {$memberData['empresa']} na ABRADi com base nas informações preenchidas no formulário de associação do nosso site. Por favor leia e assine digitalmente para avançarmos.\n\nNosso departamento de marketing publicará a {$memberData['empresa']} em abradi.com.br/associados assim que nos assinar o termo. Vc receberá, ainda, nosso Manual do Novo Associado com o selo 'Associado ABRADi para aplicação nos seus materiais de comunicação.\n\nCaso tenha alguma dúvida estarei a sua disposição no telefone 0800.878.1212 ou, se preferir, no e-mail leandro.rodrigues@abradi.com.br.\n\nBem-vindo ao time!"
            ];

            $req = curl_init(
                KEYS['CLICKSIGN_API_URL']
                . '?access_token=' .
                KEYS['CLICKSIGN_API_TOKEN']
            );

            curl_setopt_array($req, [
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => json_encode($adhesionTerm),
                CURLOPT_HTTPHEADER => [
                    'Host: app.clicksign.com',
                    'Accept: application/json',
                    'Content-Type: application/json',
                ]
            ]);

            curl_exec($req);
            curl_close($req);

            addRDStationLead(KEYS['RDSTATION_API_TOKEN'], $memberData);

            return new WP_REST_Response([
                'status' => 201,
                'message' => 'Obrigado por se cadastrar, estamos preparando seu termo de adesão'
            ]);
        }
        return new WP_REST_Response([
            'status' => 400,
            'message' => 'Ops, este usuário já é um associado ABRADi'
        ]);
    } catch (\Throwable $th) {
        return new WP_REST_Response([
            'status' => 400,
            'message' => 'Ops, um erro inesperado ocorreu ao se cadastrar'
        ]);
    }
}

<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Fetch all cities
 *
 * @param WP_REST_Request $request
 * @return WP_REST_Response
 */
function getCities(WP_REST_Request $request): WP_REST_Response
{
    global $wpdb;
    $estadoId = $request->get_params()['estado'];

    $cidades = $wpdb->get_results('
        SELECT id, nome FROM al_cidades WHERE estado LIKE '.$estadoId.'
        ORDER BY al_cidades.capital IS NULL, nome ASC'
    );

    $cidades = json_encode($cidades);
    $cidades = preg_replace_callback(
        '/\\\\u([0-9a-fA-F]{4})/',
        function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UTF-16BE');
        },
        $cidades
    );

    if (!empty($cidades)) {
        return new WP_REST_Response([
            'status' => 200,
            'cidades' => $cidades,
            'message' => 'Successfully obtained resource'
        ]);
    }

    return new WP_REST_Response([
        'status' => 500,
        'message' => 'Error trying to get resource'
    ]);
}

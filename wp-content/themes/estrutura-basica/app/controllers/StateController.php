<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Fetch all states
 *
 * @return WP_REST_Response
 */
function getStates(): WP_REST_Response
{
    global $wpdb;
    $estados = $wpdb->get_results('SELECT id, uf FROM al_estados');

    if (!empty($estados)) {
        return new WP_REST_Response([
            'status' => 200,
            'estados' => $estados,
            'message' => 'Successfully obtained resource'
        ]);
    }

    return new WP_REST_Response([
        'status' => 500,
        'message' => 'Error trying to get resource'
    ]);
}

<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get "comites" posts for archive-comites.php
 *
 * @return WP_Query
 */
function archiveComitesQuery(): WP_Query
{
    return new WP_Query([
        'post_type' => 'comites',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
    ]);
}

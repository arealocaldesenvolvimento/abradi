<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get recent posts for page-noticias.php
 *
 * @param string $regionalSlug
 * @return WP_Query
 */
function pageNoticiasRecentNewsQuery(string $regionalSlug): WP_Query
{
    $recentNews = new WP_Query([
        'post_type' => 'post',
        'posts_per_page' => 5,
        'orderby' => 'date',
        'order' => 'DESC',
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'noticias',
            ],
            [
                'relation' => 'OR',
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => $regionalSlug,
                    'compare' => 'LIKE'
                ],
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => 'nacional',
                    'compare' => 'LIKE'
                ],
            ],
        ],
    ]);

    return $recentNews;
}

/**
 * Get posts with filters for page-noticias.php
 *
 * @param string $postSearchQuery
 * @param string $regionalSlug
 * @return stdClass
 */
function pageNoticiasFilteredNewsQuery(string $postSearchQuery, string $regionalSlug): stdClass
{
    $currentPage = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $newsPerPage = 8;

    $args = [
        'post_type' => 'post',
        'posts_per_page' => $newsPerPage,
        'orderby' => 'date',
        'order' => 'DESC',
        'paged' => $currentPage,
        'tax_query' => [
            'relation' => 'AND',
            [
                'relation' => 'OR',
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => $regionalSlug,
                    'compare' => 'LIKE'
                ],
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => 'nacional',
                    'compare' => 'LIKE'
                ],
            ],
        ],
    ];

    if ($categorySlug = get_category(get_queried_object_id())->slug)
        $args['tax_query'][] = [
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms' => $categorySlug,
        ];


    if ($postSearchQuery)
        $args += [
            's' => $postSearchQuery
        ];

    $news = new stdClass;
    $news->results = new WP_Query($args);
    $news->perPage = $newsPerPage;

    return $news;
}

<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get related posts of comite for single-comites.php
 *
 * @param string $regionalSlug
 * @return WP_Query
 */
function singleComitesQuery(string $regionalSlug): WP_Query
{
    global $post;

    return new WP_Query([
        'post_type' => 'post',
        'posts_per_page' => 3,
        'orderby' => 'date',
        'order' => 'DESC',
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $post->post_name,
                'compare' => 'LIKE'
            ],
            [
                'relation' => 'OR',
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => $regionalSlug,
                    'compare' => 'LIKE'
                ],
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => 'nacional',
                    'compare' => 'LIKE'
                ],
            ],
        ],
    ]);
}

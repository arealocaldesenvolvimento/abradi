<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get "regionais" posts for archive-regionais.php
 *
 * @return WP_Query
 */
function archiveRegionaisQuery(): WP_Query
{
    return new WP_Query([
        'post_type' => 'regionais',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'post__not_in' => [
            GLOBALS['ID_NACIONAL']
        ]
    ]);
}

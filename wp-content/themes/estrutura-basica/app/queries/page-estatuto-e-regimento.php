<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get "estatutos-regimentos" posts for page-estatuto-e-regimento.php
 *
 * @return WP_Query
 */
function pageEstatutoERegimentoQuery(): WP_Query
{
    return new WP_Query([
        'post_type' => 'estatutos-regimentos',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
    ]);
}

<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get related posts for single.php
 *
 * @param string $regionalSlug
 * @return WP_Query
 */
function singlePageQuery(string $regionalSlug): WP_Query
{
    return new WP_Query([
        'post_type' => 'post',
        'posts_per_page' => 3,
        'post__not_in' => [get_the_ID()],
        'orderby' => 'date',
        'order' => 'DESC',
        'category_name' => get_category(get_queried_object_id())->slug,
        'tax_query' => [
            'relation' => 'OR',
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => 'nacional',
                'compare' => 'LIKE'
            ],
        ],
    ]);
}

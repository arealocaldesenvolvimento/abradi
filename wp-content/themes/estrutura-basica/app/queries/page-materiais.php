<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get "projetos" for page-materiais.php
 *
 * @return WP_Query
 */
function pageMateriaisQuery(): WP_Query
{
    return new WP_Query([
        'post_type' => 'projetos',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
    ]);
}

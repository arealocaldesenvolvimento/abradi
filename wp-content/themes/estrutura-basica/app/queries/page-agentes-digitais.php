<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get users with filters for page-agentes-digitais.php
 *
 * @param string $userSearchQuery
 * @return stdClass
 */
function pageAgentesDigitaisQuery(string $userSearchQuery, string $regionalUf): stdClass
{
    global $wpdb;
    $wpdb->get_results('SET SQL_BIG_SELECTS=1');

    $searchValuesToDisplay = [];

    $currentPage = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $usersPerPage = 16;

    $args = [
        'role' => 'membro',
        'order' => 'ASC',
        'orderby' => 'order_clause',
        'number' => $usersPerPage,
        'paged' => $currentPage,
        'meta_query' => [
            'order_clause' => [
                'key' => 'empresa_membro'
            ],
        ],
    ];

    if ($userSearchQuery) {
        $args += [
            'search_columns' => [
                'user_login',
                'user_nicename',
                'user_email',
                'user_url',
                'first_name',
                'last_name',
                'display_name',
            ],
            'search' => '*' . str_replace('-', '', sanitize_title($userSearchQuery)) . '*',
        ];

        $searchValuesToDisplay[] = $userSearchQuery;
    }

    if ($_GET['estado']) {
        $args['meta_query'][] = [
            'relation' => 'AND',
            [
                'key' => 'estado_membro',
                'value' => $_GET['estado'] !== 'todos' ? $_GET['estado'] : '',
                'compare' => 'LIKE'
            ],
        ];

        $searchValuesToDisplay[] = $_GET['estado'];
    } else {
        switch ($regionalUf) {
            case 'Nacional':
                break;

            case 'Norte':
                $args += [
                    'meta_query' => [
                        'relation' => 'AND',
                        [
                            'key' => 'estado_membro',
                            'value' => ['AM', 'AP', 'AC', 'PA', 'RR', 'RO', 'TO'],
                            'compare' => 'IN',
                        ],
                    ]
                ];
            break;

            default:
                $args += [
                    'meta_query' => [
                        'relation' => 'AND',
                        [
                            'key' => 'estado_membro',
                            'value' => $regionalUf,
                            'compare' => 'LIKE',
                        ],
                    ]
                ];
            break;
        }
    }

    if ($_GET['segmento']) {
        $args['meta_query'][] = [
            'key' => 'area_atuacao',
            'value' => $_GET['segmento'],
            'compare' => 'LIKE',
        ];

        $searchValuesToDisplay[] = get_post($_GET['segmento'])->post_title;
    }

    $users = new stdClass;
    $users->results = new WP_User_Query($args);
    $users->perPage = $usersPerPage;
    $users->searchValuesToDisplay = $searchValuesToDisplay;

    return $users;
}

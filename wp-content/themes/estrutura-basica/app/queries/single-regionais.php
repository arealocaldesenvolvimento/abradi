<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get highlights posts for single-regionais.php
 *
 * @param string $regionalSlug
 * @return WP_Query
 */
function singleRegionaisHighlightsQuery(string $regionalSlug): WP_Query
{
    $highlights = new WP_Query([
        'post_type' => 'post',
        'posts_per_page' => 5,
        'orderby' => 'date',
        'order' => 'DESC',
        'relation' => 'AND',
        'meta_query' => [
            [
                'key' => 'destaque',
                'value' => true,
                'compare' => '='
            ],
        ],
        'tax_query' => [
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
        ],
    ]);

    if ($highlights->post_count === 0) {
        $highlights = new WP_Query([
            'post_type' => 'post',
            'posts_per_page' => 5,
            'orderby' => 'date',
            'order' => 'DESC',
            'tax_query' => [
                'relation' => 'OR',
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => $regionalSlug,
                    'compare' => 'LIKE'
                ],
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => 'nacional',
                    'compare' => 'LIKE'
                ],
            ],
        ]);
    }

    return $highlights;
}

/**
 * Get news posts for single-regionais.php
 *
 * @param string $regionalSlug
 * @return WP_Query
 */
function singleRegionaisNewsQuery(string $regionalSlug): WP_Query
{
    return new WP_Query([
        'post_type' => 'post',
        'posts_per_page' => 4,
        'orderby' => 'date',
        'order' => 'DESC',
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'noticias',
            ],
            [
                'relation' => 'OR',
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => $regionalSlug,
                    'compare' => 'LIKE'
                ],
                [
                    'taxonomy' => 'regionais',
                    'field' => 'slug',
                    'terms' => 'nacional',
                    'compare' => 'LIKE'
                ],
            ],
        ],
    ]);
}

/**
 * Get most recent events for single-regionais.php
 *
 * @param string $regionalSlug
 * @return WP_Query
 */
function singleRegionaisEventsQuery(string $regionalSlug): WP_Query
{
    return new WP_Query([
        'post_type' => 'agenda',
        'posts_per_page' => 5,
        'orderby' => 'data',
        'order' => 'ASC',
        'meta_query' => [
            'relation' => 'AND',
            [
                'key' => 'data',
                'value' => date('Ymd', strtotime(date('Y-m-d'))),
                'compare' => '>='
            ]
        ],
        'tax_query' => [
            'relation' => 'OR',
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => 'nacional',
                'compare' => 'LIKE'
            ],
        ],
    ]);
}

/**
 * Get most recent events for single-regionais.php
 *
 * @param string $regionalSlug
 * @return WP_Query
 */
function singleRegionaisContributorsQuery(string $regionalSlug): WP_Query
{
    return new WP_Query([
        'post_type' => 'colaboradores',
        'posts_per_page' => -1,
        'orderby' => 'rand',
        'tax_query' => [
            'relation' => 'OR',
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => 'nacional',
                'compare' => 'LIKE'
            ],
        ]
    ]);
}

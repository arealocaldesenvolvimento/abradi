<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get events with filters for page-eventos.php
 *
 * @param string $eventSearchQuery
 * @return stdClass
 */
function pageEventosQuery(string $eventSearchQuery): stdClass
{
    $currentPage = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $eventsPerPage = 5;

    $defaults = [
        'fields' => 'ids',
        'posts_per_page' => -1,
        'post_type' => 'agenda',
        'orderby' => 'data',
        'order' => 'ASC',
    ];

    $argsHappened = array_merge($defaults, [
        'meta_query' => [
            'relation' => 'AND',
            [
                'key' => 'data',
                'value' => date('Ymd', strtotime(date('Y-m-d'))),
                'compare' => '<='
            ],
            [
                'key' => 'data',
                'value' => date('Y', strtotime(date('Y'))),
                'compare' => 'LIKE'
            ],
        ],
    ]);

    $argsWillHappen = array_merge($defaults, [
        'meta_query' => [
            'relation' => 'AND',
            [
                'key' => 'data',
                'value' => date('Ymd', strtotime(date('Y-m-d'))),
                'compare' => '>='
            ]
        ],
    ]);

    if ($_GET['estado']) {
        $argsWillHappen['tax_query'] = [
            [
                'taxonomy' => 'regionais',
                'field' => 'name',
                'terms' => $_GET['estado'],
                'compare' => 'LIKE',
            ],
        ];

        $argsHappened['tax_query'] = [
            [
                'taxonomy' => 'regionais',
                'field' => 'name',
                'terms' => $_GET['estado'],
                'compare' => 'LIKE',
            ],
        ];
    }

    if ($eventSearchQuery) {
        $argsWillHappen += [
            's' => $eventSearchQuery
        ];
    }

    if ($_GET['de'] || $_GET['ate']) {
        $from = $_GET['de'];
        $to = $_GET['ate'];

        $argsWillHappen['meta_query'][] = [
            'relation' => 'AND',
            $from ? [
                'key' => 'data',
                'compare' => '>=',
                'value' => getFormatedDate($from),
                'type' => 'DATE'
            ] : '',
            $to ? [
                'key' => 'data',
                'compare' => '<=',
                'value' => getFormatedDate($to),
                'type' => 'DATE'
            ] : ''
        ];

        $argsHappened['meta_query'][] = [
            'relation' => 'AND',
            $from ? [
                'key' => 'data',
                'compare' => '>=',
                'value' => getFormatedDate($from),
                'type' => 'DATE'
            ] : '',
            $to ? [
                'key' => 'data',
                'compare' => '<=',
                'value' => getFormatedDate($to),
                'type' => 'DATE'
            ] : ''
        ];
    }

    $happened = get_posts(array_merge($defaults, $argsHappened));
    $willHappen = get_posts(array_merge($defaults, $argsWillHappen));

    $postIds = array_merge($willHappen, $happened);

    $eventQuery = new WP_Query([
        'post_type' => 'agenda',
        'post__in' => $postIds,
        'order' => 'ASC',
        'orderby'=> 'post__in',
        'posts_per_page' => $eventsPerPage,
        'paged' => $currentPage,
    ]);

    $events = new stdClass;
    $events->results = $eventQuery;
    $events->perPage = $eventsPerPage;

    return $events;
}

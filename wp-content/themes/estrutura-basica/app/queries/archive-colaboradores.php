<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get "estatutos-regimentos" posts for page-estatuto-e-regimento.php
 *
 * @param string $regionalSlug
 * @return WP_Query
 */
function archiveColaboradoresQuery(string $regionalSlug): WP_Query
{
    return new WP_Query([
        'post_type' => 'colaboradores',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'tax_query' => [
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
        ],
    ]);
}

<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get "diretores" posts for page-diretoria.php
 *
 * @param string $regionalSlug
 * @param string $management
 * @return stdClass
 */
function pageDiretoriaQueries(string $regionalSlug, string $management): stdClass
{
    $directors = new WP_Query([
        'post_type' => 'diretores',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'cargos',
                'field' => 'slug',
                'terms' => 'diretores',
            ],
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
            $management ? [
                'taxonomy' => 'gestoes',
                'field' => 'slug',
                'terms' => $management,
            ] : ''
        ]
    ]);

    $executives = new WP_Query([
        'post_type' => 'diretores',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'cargos',
                'field' => 'slug',
                'terms' => 'corpo-executivo',
            ],
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
            $management ? [
                'taxonomy' => 'gestoes',
                'field' => 'slug',
                'terms' => $management,
            ] : ''
        ]
    ]);

    $directorsQueries = new stdClass;

    $directorsQueries->directors = $directors;
    $directorsQueries->executives = $executives;

    return $directorsQueries;
}

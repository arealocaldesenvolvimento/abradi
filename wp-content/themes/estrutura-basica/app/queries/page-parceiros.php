<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get "materiais-parceiros" for page-parceiros.php
 *
 * @return WP_Query
 */
function pageMateriaisParceirosQuery(): WP_Query
{
    return new WP_Query([
        'post_type' => 'materiais-parceiros',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
    ]);
}

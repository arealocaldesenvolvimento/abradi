<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get "convenio" posts for page-convenios-e-beneficios.php
 *
 * @param WP_Term $term
 * @return WP_Query
 */
function pageConveniosEBeneficiosQuery(WP_Term $term): WP_Query
{
    return new WP_Query([
        'post_type' => 'convenio',
        'posts_per_page' => -1,
        'tax_query' => [
            [
                'taxonomy' => 'categoria_convenio',
                'field' => 'slug',
                'terms' => $term->slug,
            ],
        ],
    ]);
}

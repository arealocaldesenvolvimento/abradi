<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get filter posts, eventos, convenios for search.php
 *
 * @param string $search
 * @return WP_Query
 */
function searchQuery(string $search): WP_Query
{
    global $wpdb;
    $wpdb->get_results('SET SQL_BIG_SELECTS=1');

    return new WP_Query([
        'post_type' => [
            'post',
            'eventos',
            'convenio',
        ],
        'posts_per_page' => 6,
        'orderby' => 'date',
        'order' => 'DESC',
        'paged' => get_query_var('paged') ? absint(get_query_var('paged')) : 1,
        's' => $search,
    ]);
}


<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Send welcome mail to a member, with credentials
 *
 * @param string $name
 * @param string $email
 * @param string $login
 * @param string $password
 * @return void
 */
function sendMailMember(string $name, string $email, string $login, string $password): void
{
    $urlPanel = home_url('/wp-admin');
    // $urlTutorial = '';

    $body = "
        <html>
            <head>
                <style>
                    h3 {
                        color: #49a3db !important;
                        font-family: sans-serif !important;
                        font-size: 1.375rem !important;
                    }
                    li,
                    p {
                        color: #333 !important;
                        font-family: sans-serif !important;
                        font-size: 1rem !important;
                    }
                    a {
                        color: #49a3db !important;
                        font-family: sans-serif !important;
                        font-size: 1rem !important;
                        text-decoration: underline !important;
                    }
                </style>
            </head>
            <body>
                <h3>Olá $name</h3>
                <p>Recebemos a sua solicitação de associação.</p>
                <p>Abaixo se encontram suas credenciais de acesso a sua conta, recomendamos que você atualize sua senha por questões de segurança. Também deixamos um link de um pequeno tutorial, para você poder atualizar suas informações de associado.</p>
                <p>
                    <ul>
                        <li>Login: $login</li>
                        <li>Senha: $password</li>
                    </ul>
                </p>
                <div>
                    <a href='$urlPanel' target='_blank'>Link para acessar sua conta</a>
                </div>
            </body>
        </html>
    ";

    wp_mail($email, "Seja Bem-Vindo à ABRADi", $body, '', 'Content-Type: text/html; charset=UTF-8');
}

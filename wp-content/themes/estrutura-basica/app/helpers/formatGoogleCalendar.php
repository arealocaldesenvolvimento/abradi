<?php

/**
 * Format url path for google calendar API
 *
 * @param string $title
 * @param string $startDate
 * @param string $endDate
 * @param string $location
 * @param string $details
 * @return string
 */
function formatGoogleCalendar(string $title, string $startDate, string $endDate, string $location, string $details): string
{
    $startTime = DateTime::createFromFormat('Ymd', $startDate)->getTimestamp();
    $endTime = DateTime::createFromFormat('Ymd', $endDate)->modify('+ 1 day')->getTimestamp();

    $startDate = str_replace(['-', '/', ':'], '',  date('Y-m-d', $startTime));
    $endDate = str_replace(['-', '/', ':'], '',  date('Y-m-d', $endTime));

    return "https://www.google.com/calendar/render?action=TEMPLATE&text=$title&dates=$startDate/$endDate&details=$details&location=$location";
}

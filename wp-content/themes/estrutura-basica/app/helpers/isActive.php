<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Adiciona a classe active nos link /category
 *
 * @param string $link
 * @return string|null
 */
function isActive(string $link): ?string
{
    global $wp;
    $current_url = home_url(add_query_arg([], $wp->request));

    if (strpos($current_url, "/categorias/$link") || strpos($current_url, "/gestoes/$link") || strpos($current_url, "/$link"))
        return 'active';

    return null;
}

<?php

/**
 * Format date to string for form calendar search
 *
 * @param string $value
 * @param string $currentFormat
 * @param string $outputFormat
 * @return string
 */
function getFormatedDate(string $value, string $currentFormat = 'd/m/Y', $outputFormat = 'Ymd'): string
{
    return preg_replace(
        '/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s',
        '',
        date($outputFormat, DateTime::createFromFormat($currentFormat, $value)->getTimestamp())
    );
}

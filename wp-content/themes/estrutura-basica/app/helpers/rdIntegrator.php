<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Handle the request to RD Station lead API
 *
 * @param string $token
 * @param array $memberData
 * @param string $identifier
 */
function addRDStationLead(string $token, array $memberData, string $identifier = 'associe-se')
{
    $memberData['token_rdstation'] = $token;
    $memberData['identificador'] = $identifier;

    if (empty($memberData['c_utmz']))
        $memberData['c_utmz'] = $_COOKIE['__utmz'];

    $req = curl_init('https://www.rdstation.com.br/api/1.3/conversions');

    curl_setopt_array($req, [
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => http_build_query($memberData),
        CURLOPT_RETURNTRANSFER => true,
    ]);

    $res = curl_exec($req);
    curl_close($req);

    return $res;
}

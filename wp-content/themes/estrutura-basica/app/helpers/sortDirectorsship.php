<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Sort the directorsship by most higher role:
 * ("presidente", "vice-presidente", "segundo vice-presidente"...)
 *
 * @param string $regionalSlug
 * @param string $regionalSlug
 * @return array
 */
function sortDirectorsship(string $regionalSlug, string $management)
{
    $directorsship = new WP_Query([
        'post_type' => 'diretores',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'DESC',
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'cargos',
                'field' => 'slug',
                'terms' => 'diretoria',
            ],
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
            $management ? [
                'taxonomy' => 'gestoes',
                'field' => 'slug',
                'terms' => $management,
            ] : ''
        ]
    ]);

    $sortedDirectorsship = [];
    $countKey = 0;
    $sortKey = [];

    while ($directorsship->have_posts()) {
        $directorsship->the_post();

        $diretor = [
            'name' => get_the_title(),
            'role' => get_field('cargo'),
            'company' => get_field('empresa'),
            'degree' => get_field('formacoes'),
            'thumb' => setPostThumbnail()
        ];

        if (substr($diretor['role'], 0, 15) === 'Vice Presidente' || substr($diretor['role'], 0, 10) === 'Presidente')
            $diretor['higher'] = true;

        $sortedDirectorsship[] = $diretor;
        $sortKey[$countKey] = $diretor['role'];
        $countKey ++;
    }

    array_multisort($sortKey, SORT_ASC, $sortedDirectorsship);

    $higherPresidents = array_filter($sortedDirectorsship, function (array $array): ?bool {
        return $array['higher'];
    }, ARRAY_FILTER_USE_BOTH);

    foreach (array_keys($higherPresidents) as $higherPresidentsKey)
        unset($sortedDirectorsship[$higherPresidentsKey]);

    return array_merge($higherPresidents, $sortedDirectorsship);
}

<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';
include_once get_theme_file_path('/vendor/autoload.php');

use \Mpdf\Mpdf;

/**
 * Builds a PDF template converting her to base64 format for Clicksign API
 *
 * @param array $member
 * @return string
 */
function generatePdf(array $member): string
{
    $mpdf = new Mpdf();
    $url = get_template_directory();

    $mpdf->WriteHTML('
        <link rel="stylesheet" href="'.$url.'/assets/css/termo-adesao-clicksign.css">
        <img src="'.$url.'/assets/images/clicksign/clicksignpg01.png">
        <p class="razaoSocial">'.$member['empresa'].'</p>
        <p class="nomeFantasia">'.$member['nomeFantasia'].'</p>
        <p class="cnpjMf">'.$member['cnpj'].'</p>
        <p class="endereco">'.$member['endereco'].' / '.$member['complemento'].'</p>
        <p class="cidadeUfCep">'.$member['cidade'].' / '.$member['estado'].' / '.$member['cep'].'</p>
        <p class="telefone">'.$member['telefone'].'</p>
        <p class="website">'.$member['site'].'</p>
        <p class="representanteLegal">'.$member['representanteLegal'].'</p>
        <p class="cargo">'.$member['cargo'].'</p>
        <p class="email">'.$member['email'].'</p>
        <p class="telefoneCelular">'.$member['celular'].'</p>
        <p class="emailCobranca">'.$member['emailCobranca'].'</p>
        <p class="mensalidade">'.$member['mensalidade'].'</p>
        <img src="'.$url.'/assets/images/clicksign/clicksignpg02.png">
        <img src="'.$url.'/assets/images/clicksign/clicksignpg03.png">
        <img src="'.$url.'/assets/images/clicksign/clicksignpg04.png">
    ');

    return base64_encode($mpdf->Output('', \Mpdf\Output\Destination::STRING_RETURN));
}

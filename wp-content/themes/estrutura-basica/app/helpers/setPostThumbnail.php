<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Get post thumbnail (only inside WP_Query loop context)
 *
 * @param string $size
 * @return string
 */
function setPostThumbnail(string $size = GLOBALS['THUMB_SIZE']): string
{
    if (get_the_post_thumbnail_url()) {
        $thumbUrl = get_the_post_thumbnail_url(null, $size);
        $thumbAlt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);

        return '<img src="'.$thumbUrl.'" alt="'.$thumbAlt.'" />';
    } else {
        $thumbUrl = get_image_url('default-thumbnail.jpg');

        return '<img src="'.$thumbUrl.'" alt="Imagem destacada padrão para postagens ABRADi" />';
    }
}

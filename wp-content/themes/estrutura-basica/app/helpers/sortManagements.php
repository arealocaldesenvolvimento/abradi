<?php

/**
 * Imports
 */
include_once SITE_ROOT . '/wp-load.php';

/**
 * Sort managements for "page-diretoria" by years: (2009/2011, 2012/2014, 2014/2016...)
 *
 * @param string $regionalSlug
 * @return array
 */
function sortManagements(string $regionalSlug): array
{
    $regionalDirectors = new WP_Query([
        'post_type' => 'diretores',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
        'fields' => 'id',
        'tax_query' => [
            [
                'taxonomy' => 'regionais',
                'field' => 'slug',
                'terms' => $regionalSlug,
                'compare' => 'LIKE'
            ],
        ]
    ]);

    $managements = [];

    while ($regionalDirectors->have_posts()) {
        $regionalDirectors->the_post();

        foreach (wp_get_post_terms(get_the_ID(), 'gestoes') as $term) {
            $managements[] = [
                'name' => $term->name,
                'slug' => $term->slug,
            ];
        }
    }

    $managements = array_unique($managements, SORT_REGULAR);

    sort($managements, SORT_DESC);

    return array_reverse($managements);
}

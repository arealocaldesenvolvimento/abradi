<?php

/**
 * Remove some crap characteres from text
 *
 * @param string $text
 * @return string
 */
function sanitizeString(string $text): string
{
    return str_replace(
        ['[:pt]', '[:en]', '[:es]', '[:]', '[;]', '[...]', '#', '[embed]', '[/embed]', '[/caption]'],
        '',
        $text
    );
}

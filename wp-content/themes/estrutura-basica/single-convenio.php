<?php get_header() ?>

    <section class="section-single-agentes-digitais">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <?php while (have_posts()): the_post() ?>
                <h1 class="section-title-01"><?= get_the_title() ?></h1>
                <?php if ($keywords = get_field('palavras_chave')): ?>
                    <span><?= $keywords ?></span>
                <?php endif ?>
                <div class="content">
                    <div class="text">
                        <?= setPostThumbnail() ?>
                        <p><?= sanitizeString(get_the_content()) ?></p>
                    </div>
                    <div class="video">
                        <?php if ($video = get_field('video')): ?>
                            <iframe src="<?= $video ?>"></iframe>
                        <?php endif ?>
                    </div>
                </div>
                <?php if (have_rows('competencias')): ?>
                    <div class="competencias">
                        <h2 class="section-title-01">Competências</h2>
                        <?php while (have_rows('competencias')): the_row() ?>
                            <div class="competencia">
                                <h3><?= get_sub_field('competencia') ?></h3>
                                <p><?= get_sub_field('descricao') ?></p>
                            </div>
                        <?php endwhile ?>
                    </div>
                <?php endif ?>
                <?php
                    $contactInfos = [
                        'aperto-de-maos-azul' => get_field('gerente'),
                        'facebook-azul' => get_field('facebook'),
                        'telefone-azul' => get_field('telefone'),
                        'instagram-azul' => get_field('instagram'),
                        'envelope-azul' => get_field('email'),
                        'linkedin-azul' => get_field('linkedin'),
                        'globo-azul' => get_field('site'),
                    ];
                ?>
                <?php if (!empty(array_filter($contactInfos))): ?>
                    <div class="contato">
                        <h2 class="section-title-01">Informações de Contato</h2>
                        <div>
                            <?php foreach ($contactInfos as $key => $info): ?>
                                <?php if ($info): ?>
                                    <div class="informacao">
                                        <p class="<?= $key ?>">
                                            <img
                                                src="<?= get_image_url('single-convenios/' . $key . '.svg') ?>"
                                                alt="Informação de Contato"
                                                title="Informação de Contato"
                                            />
                                            <?= $info ?>
                                        </p>
                                    </div>
                                <?php endif ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                <?php endif ?>
                <?php if ($location = get_field('localizacao') && $address = get_field('endereco')): ?>
                    <div class="localizacao">
                        <h2 class="section-title-01">Localização</h2>
                        <div class="infos">
                            <p>
                                <img
                                    src="<?= get_image_url('single-eventos/maps-azul.svg') ?>"
                                    alt="Localização"
                                    title="Localização"
                                />
                                <?= $address ?>
                            </p>
                            <a
                                href="http://maps.google.com/?q=<?= $address ?>"
                                title="Como Chegar?"
                                target="_blank"
                            >
                                Como Chegar?
                            </a>
                        </div>
                        <div class="mapa">
                            <input class="lat" type="hidden" value="<?= $location['lat'] ?>">
                            <input class="lng" type="hidden" value="<?= $location['lng'] ?>">
                            <section id="cd-google-map">
                                <div id="google-container"></div>
                                <div id="cd-zoom-in"></div>
                                <div id="cd-zoom-out"></div>
                            </section>
                        </div>
                    </div>
                <?php endif ?>
            <?php endwhile ?>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/single-convenio.js" defer></script>

<?php get_footer() ?>


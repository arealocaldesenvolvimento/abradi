<?php

get_header();

$regional = get_post($_SESSION['regional']);
$regionalSlug = $regional->post_name;
$postSearchQuery = $_GET['busca-noticia'] ?? get_search_query();

?>

    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        rel="stylesheet"
    >
    <section class="section-noticias">
        <div class="al-small-container">
            <div class="noticias-recentes">
                <nav class="breadcrumb">
                    <?= yoast_breadcrumb() ?>
                </nav>
                <h1 class="section-title-01">Notícias</h1>
                <p>Você está na ABRADi: <span><?= $regional->post_title ?></span></p>
                <?php $recentNews = pageNoticiasRecentNewsQuery($regionalSlug) ?>
                <?php if ($recentNews->have_posts()): ?>
                    <div class="noticias">
                        <?php $count = 1 ?>
                        <?php while ($recentNews->have_posts()): $recentNews->the_post() ?>
                            <?php
                                $isSmallCard = $count >= 3;
                                $title = $isSmallCard
                                    ? wp_trim_words(get_the_title(), 6)
                                    : wp_trim_words(get_the_title(), 8);
                            ?>
                            <article class="<?= $isSmallCard ? 'small' : '' ?>">
                                <a href="<?= get_permalink() ?>">
                                    <div class="image-wrapper">
                                        <?= setPostThumbnail('large') ?>
                                    </div>
                                    <div class="content">
                                        <h2><?= $title ?></h2>
                                        <span><?= get_the_date() ?></span>
                                    </div>
                                </a>
                            </article>
                        <?php $count ++; endwhile ?>
                    </div>
                <?php else: ?>
                    <div>
                        <p class="notfound-message">Não foram cadastradas notícias</p>
                    </div>
                <?php endif ?>
            </div>
            <div class="noticias-search-bar">
                <form
                    id="busca-page-noticias"
                    action="<?= home_url('/noticias') ?>"
                    method="GET"
                    role="search"
                >
                    <input
                        id="busca-noticia"
                        type="text"
                        name="busca-noticia"
                        placeholder="&#xF002;"
                        value="<?= $postSearchQuery ?? '' ?>"
                    >
                    <button type="submit">Pesquisar</button>
                </form>
                <?php
                    $categories = get_categories([
                        'orderby' => 'name',
                        'order' => 'ASC',
                        'hide_empty' => true,
                    ])
                ?>
                <nav class="tabs">
                    <ul>
                        <li class="tab">
                            <a
                                class="<?= get_query_var('pagename') === 'noticias' ? 'active' : '' ?>"
                                href="<?= get_permalink(32) ?>"
                                title="Todos"
                            >
                                Todos
                            </a>
                        </li>
                        <?php foreach ($categories as $category): ?>
                            <li class="tab">
                                <a
                                    class="<?= isActive($category->slug) ?>"
                                    id="<?= $category->slug ?>"
                                    href="<?= get_category_link($category) ?>"
                                    title="<?= $category->name ?>"
                                >
                                    <?= $category->name ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </nav>
            </div>
            <div class="noticias">
                <?php $news = pageNoticiasFilteredNewsQuery($postSearchQuery, $regionalSlug) ?>
                <?php if ($news->results->have_posts()): ?>
                    <?php while ($news->results->have_posts()): $news->results->the_post() ?>
                        <article>
                            <a href="<?= get_permalink() ?>">
                                <div class="image-wrapper">
                                    <?= setPostThumbnail('large') ?>
                                </div>
                                <div class="content">
                                    <h2><?= wp_trim_words(get_the_title(), 8)?></h2>
                                    <span><?= get_the_date() ?></span>
                                </div>
                            </a>
                        </article>
                    <?php endwhile ?>
                <?php else: ?>
                    <p class="notfound-message">
                        Não foram encontradas notícias para essa pesquisa
                    </p>
                <?php endif ?>
            </div>
            <div>
                <nav class="pagination">
                    <?= paginationLinks($news->results, $news->perPage) ?>
                </nav>
            </div>
        </div>
    </section>

<?php get_footer() ?>

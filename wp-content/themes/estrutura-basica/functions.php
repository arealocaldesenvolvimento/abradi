<?php

/**
 * Theme session
 */
session_start();

/* Queries
========================================================================== */
include_once get_theme_file_path('app/queries/single.php');
include_once get_theme_file_path('app/queries/single-regionais.php');
include_once get_theme_file_path('app/queries/page-eventos.php');
include_once get_theme_file_path('app/queries/page-noticias.php');
include_once get_theme_file_path('app/queries/page-agentes-digitais.php');
include_once get_theme_file_path('app/queries/page-materiais.php');
include_once get_theme_file_path('app/queries/page-parceiros.php');
include_once get_theme_file_path('app/queries/page-diretoria.php');
include_once get_theme_file_path('app/queries/archive-regionais.php');
include_once get_theme_file_path('app/queries/page-convenios-e-beneficios.php');
include_once get_theme_file_path('app/queries/archive-comites.php');
include_once get_theme_file_path('app/queries/search.php');
include_once get_theme_file_path('app/queries/page-estatuto-e-regimento.php');
include_once get_theme_file_path('app/queries/archive-colaboradores.php');
include_once get_theme_file_path('app/queries/single-comites.php');

/* Helpers
========================================================================== */
include_once get_theme_file_path('app/helpers/getFormatedDate.php');
include_once get_theme_file_path('app/helpers/rdIntegrator.php');
include_once get_theme_file_path('app/helpers/generatePdf.php');
include_once get_theme_file_path('app/helpers/isActive.php');
include_once get_theme_file_path('app/helpers/getMonthName.php');
include_once get_theme_file_path('app/helpers/formatGoogleCalendar.php');
include_once get_theme_file_path('app/helpers/sanitizeString.php');
include_once get_theme_file_path('app/helpers/sendMailMember.php');
include_once get_theme_file_path('app/helpers/sortManagements.php');
include_once get_theme_file_path('app/helpers/sortDirectorsship.php');
include_once get_theme_file_path('app/helpers/setPostThumbnail.php');

/* App
========================================================================== */
include_once get_theme_file_path('app/config.php');
include_once get_theme_file_path('app/scripts/post-types.php');
include_once get_theme_file_path('app/routes.php');

/* Actions & Filters
========================================================================== */
add_action('show_admin_bar', '__return_false');
add_action('admin_head', 'show_panel_favicon');
add_filter('widget_text', 'do_shortcode');
add_filter('retrieve_password_message', 'reset_password_message', null, 2);
add_action('after_setup_theme', 'remove_post_format', 15);
remove_action('wp_head', 'wp_generator');

/* Theme functions/filters/actions...
========================================================================== */
function wp_mail_return_texthtml(): string
{
    return 'text/html';
}
add_filter('wp_mail_content_type', 'wp_mail_return_texthtml');

/**
 * Records widget areas
 *
 * @return void
 */
function add_widget_areas(): void
{
    register_sidebar([
        'name' => 'Área de Widget Primária',
        'id' => 'area-widget-primaria',
        'description' => 'Área de Widget Primária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ]);

    register_sidebar([
        'name' => 'Área de Widget Secundária',
        'id' => 'area-widget-secundaria',
        'description' => 'Área de Widget Secundária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ]);
}
add_action('widgets_init', 'add_widget_areas');

/**
 * Returns the full path of the image
 *
 * @param string $file
 * @return string
 */
function get_image_url(string $file = ''): string
{
    return get_template_directory_uri() . '/assets/images/' . $file;
}

/**
 * Print the full path of the image
 *
 * @param string $file
 * @return void
 */
function image_url(string $file = ''): void
{
    echo get_image_url($file);
}

/**
 * Print the image of the logo, if it is the home place a wrap of h1
 *
 * @return void
 */
function get_logo(string $img = 'logo.png'): void
{
    $blogName = get_bloginfo('name');

    $tag = '<a href="%s" title="%s - %s" id="header-logo"><img src="%s" alt="%s" /></a>';
    $tag = ($tag) ? "<h1 id='wrap-logo'>{$tag}</h1>" : '';
    $tag = sprintf($tag, home_url('/'), $blogName, get_bloginfo('description'), get_image_url($img), $blogName);

    echo $tag;
}

/**
 * Change text at the bottom of the panel
 *
 * @return void
 */
function change_panel_footer_text(): void
{
    echo '&copy; <a href="http://www.arealocal.com.br/" target="_blank">&Aacute;rea Local</a> - Web especializada';
}
add_filter('admin_footer_text', 'change_panel_footer_text');

/**
 * Change the login form logo
 *
 * @return void
 */
function change_login_form_logo(): void
{
    echo '<style>.login h1 a{background-image:url(' . get_image_url('logo-login-form.png') . ')!important;}</style>';
}
add_action('login_enqueue_scripts', 'change_login_form_logo');

/**
 * Change the login form logo url
 *
 * @return string
 */
function login_form_logo_url(): string
{
    return get_home_url();
}
add_filter('login_headerurl', 'login_form_logo_url');

/**
 * Standardizes login error message, so as not to show when the user exists
 *
 * @return string
 */
function wrong_login(): string
{
    return '<b>ERRO</b>: Usuário ou senha incorretos.';
}
add_filter('login_errors', 'wrong_login');

/**
 * Change title of the login form logo
 *
 * @return string
 */
function login_form_logo_url_title(): string
{
    return get_bloginfo('name') . ' - Desenvolvido por Área Local';
}
add_filter('login_headertext', 'login_form_logo_url_title');

/**
 * Adds main navigation, html5 support and post thumbnail
 *
 * @return void
 */
function al_setup(): void
{
    register_nav_menus(['principal' => 'Navegação Principal']);

    add_theme_support('post-thumbnails');
    add_theme_support('html5', ['comment-list', 'comment-form', 'search-form', 'gallery', 'caption']);
}
add_action('after_setup_theme', 'al_setup');

/**
 * Add home in the menu
 *
 * @param array $args
 * @return array
 */
function show_home_menu(array $args): array
{
    $args['show_home'] = true;
    return $args;
}
add_filter('wp_page_menu_args', 'show_home_menu');

/**
 * Disables wordpress emojis
 *
 * @return void
 */
function disable_wp_emojicons(): void
{
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
}
add_action('init', 'disable_wp_emojicons');

/**
 * Corrects accent file upload bug
 *
 * @param string $filename
 * @return string
 */
function sanitize_filename(string $filename): string
{
    $ext = explode('.', $filename);
    $ext = end($ext);
    $sanitized = preg_replace('/[^a-zA-Z0-9-_.]/', '', substr($filename, 0, - (strlen($ext) + 1)));
    $sanitized = str_replace('.', '-', $sanitized);

    if (function_exists('sanitize_title'))
        $sanitized = sanitize_title($sanitized);

    return strtolower($sanitized . '.' . $ext);
}
add_filter('sanitize_file_name', 'sanitize_filename', 10);

/**
 * Changes the default excerpt size in words
 *
 * @return int
 */
function change_excerpt_length(): int
{
    return 40;
}
add_filter('excerpt_length', 'change_excerpt_length');

/**
 * Configures the Google Maps JS API key
 *
 * @return void
 */
function setGoogleMapsKey(): void
{
    acf_update_setting('google_api_key', KEYS['GOOGLE_MAPS_API_KEY']);
}
add_action('acf/init', 'setGoogleMapsKey');

/**
 * Brings the thumbnail, post image or standard image
 *
 * @param int $post_id
 * @param string $size
 * @return string
 */
function getThumbnailUrl(int $post_id, string $size): string
{
    if (!isset($post_id))
        $post_id = get_the_ID();

    if (has_post_thumbnail($post_id)) {
        $post_thumbnail_url = get_the_post_thumbnail_url('', $size);
    } else {
        $args = [
            'numberposts' => 1,
            'order' => 'ASC',
            'post_mime_type' => 'image',
            'post_parent' => $post_id,
            'post_status' => null,
            'post_type' => 'attachment',
        ];
        $post_thumbnail_url = array_values(get_children($args))[0]->guid;

        if (empty($post_thumbnail_url))
            $post_thumbnail_url = get_image_url('default.png');
    }

    return $post_thumbnail_url;
}

/**
 * Adequate pagination for single-post and archive-post
 *
 * @param WP_Query $query
 * @return void
 */
function customPostsPerPage(WP_Query $query): void
{
    global $pagenow;

    if ($query->is_archive('post') && $pagenow !== 'edit.php')
        set_query_var('posts_per_page', 1);
}
add_action('pre_get_posts', 'customPostsPerPage');

/**
 * Site redirects/template queries
 *
 * @param string $template
 */
function themeRedirects(string $template)
{
    if (is_tax('gestoes')) {
        $template = get_query_template('page-diretoria');
    } else if (is_category()) {
        $template = get_query_template('page-noticias');
    } else if (is_page('artigos')) {
        return wp_safe_redirect(home_url('/categorias/artigos'));
    } else if (is_page('diretoria')) {
        $regionalSlug = get_post($_SESSION['regional'])->post_name;
        $lastManagement = sortManagements($regionalSlug)[0]['slug'];

        if ($lastManagement) {
            return wp_safe_redirect(home_url("/gestoes/$lastManagement"));
        }
    }

    return $template;
}
add_filter('template_include', 'themeRedirects');

/**
 * Creates a "regional" term when a "Regional" post-type is created
 *
 * @param int $id
 * @param WP_Post $regional
 * @return void
 */
function createRegionalTerms(int $id, WP_Post $regional): void
{
    $title = trim($regional->post_title);

    wp_insert_term(
        $title,
        'regionais',
        [
            'description' => "Regional $title",
            'slug' => strtolower(GLOBALS['STATES'][$title]),
        ]
    );
}
add_action('publish_regionais', 'createRegionalTerms', 10, 2);

/**
 * Handles the site menus before its render
 *
 * @param string $items
 * @param stdClass $args
 * @return string
 */
function beforeRenderSiteMenus(string $items, stdClass $args): string
{
    if ($args->menu === 'Header01') {
        $regionalUf = get_field('estado', $_SESSION['regional']) === 'Nacional'
            ? ''
            : get_field('estado', $_SESSION['regional']);

        $abradiPagelink = get_permalink(378);
        $directorsPagelink = get_permalink(26);

        $subMenu = '
            <li>
                <span>Abradi '.$regionalUf.'</span>
                <ul class="header01-sub-menu">
                    <li>
                        <a href="'.$abradiPagelink.'">A Associação</a>
                    </li>
                    <li>
                        <a href="'.$directorsPagelink.'">Diretoria</a>
                    </li>
                </ul>
            </li>
        ';

        $items = $subMenu . $items;
    }

    if ($args->menu === 'Header02') {
        $items .= '
            <li>
                <a href="'.home_url('/wp-admin').'" title="Acessar painel do associado">
                    Login do Associado
                </a>
            </li>
        ';
    }

    if ($args->menu === 'Footer04') {
        $items = "<li>0800 878 1212</li>" . $items;
    }

    return $items;
}
add_filter('wp_nav_menu_items', 'beforeRenderSiteMenus', 10, 2);

/**
 * Render Área Local help panel
 */
function customAlHelpBox(): void
{
    echo '
		<p>
			Bem-vindo ao tema Área Local! precisa de ajuda?</br> Contate o suporte
			<a target="_blank" href="https://suporte-arealocal.tomticket.com/">aqui!</a>
		</p>
		<h2>Contato</h2>
		<p>Telefone/Whastapp: <a target="_blank" href="https://wa.me/554735219850"><b>(47) 3521-9850</b></a></p>
		<p>E-mail:
			<a target="_blank" href="mailto:contato@arealocal.com.br"><b>contato@arealocal.com.br</b></a>
		</p>
	';
}

/**
 * Enables service area on the Wordpress dashboard
 *
 * @return void
 */
function customWidgets(): void
{
    global $wp_meta_boxes;
    wp_add_dashboard_widget('atendimento-arealocal', 'Atendimento Área Local', 'customAlHelpBox');
}
add_action('wp_dashboard_setup', 'customWidgets');

/**
 * Add scripts & styles in admin panel
 *
 * @return void
 */
function adminScripts(): void
{
    wp_enqueue_style('admin-styles', get_bloginfo('template_url') . '/assets/css/admin.css');
    wp_register_script('admin-js', get_bloginfo('template_url') . '/assets/js/admin.js');
    wp_enqueue_script('admin-js');
}
add_action('admin_enqueue_scripts', 'adminScripts');

/**
 * Change admin panel color scheme
 */
wp_admin_css_color(
    'area-structure-4',
    __('Área Structure 4'),
    get_bloginfo('template_url') . '/assets/css/al-admin-color-scheme.min.css',
    [
        '#222',
        '#e0e047',
        '#00244c',
        '#19539a'
    ],
    [
        'base' => '#e5f8ff',
        'focus' => '#fff',
        'current' => '#fff'
    ]
);

/**
 * Set "area-structure-4" as default color scheme
 *
 * @param int $userId
 * @return void
 */
function setDefaultAdminColor(int $userId): void
{
    wp_update_user([
        'ID' => $userId,
        'admin_color' => 'area-structure-4'
    ]);
}
add_action('user_register', 'setDefaultAdminColor');

/**
 * Sets the term "national" by default for those posts that do not have a "regional" term selected
 *
 * @param int $post_id
 * @param WP_Post $post
 * @return void
 */
function defaultTermForPosts(int $post_id, WP_Post $post): void
{
    if ($post->post_status === 'publish' && ($post->post_type === 'post' || $post->post_type === 'agenda')) {
        $nationalTermId = get_term_by('slug', 'nacional', 'regionais')->term_id;
        $regionalTerm = wp_get_post_terms($post_id, 'regionais');

        if (empty($regionalTerm))
            wp_set_object_terms($post_id, $nationalTermId, 'regionais');
    }
}
add_action('save_post', 'defaultTermForPosts', 100, 2);

/**
 * Change url for agencies (users)
 *
 * @return void
 */
function rewriteAgenciesUrl(): void
{
    global $wp_rewrite;

    $wp_rewrite->author_base = 'agente';
    $wp_rewrite->flush_rules();
}
add_action('init', 'rewriteAgenciesUrl');

/**
 * Remove "/regionais/" from regionais post-type url
 *
 * @param string $post_link
 * @param WP_Post $post
 * @param string|null $leavename
 * @return string
 */
function changeRegionaisUrlSlug(string $post_link, WP_Post $post, ?string $leavename): string
{
    if ('regionais' !== $post->post_type || 'publish' !== $post->post_status)
        return $post_link;

    $post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);

    return $post_link;
}
add_filter('post_type_link', 'changeRegionaisUrlSlug', 10, 3);

/**
 * Includes one of them: post, regionais & page post-types in empty queries
 *
 * @param string $query
 * @return void
 */
function setEmptyQueries(WP_Query $query): void
{
    if (!$query->is_main_query() || 2 !== count($query->query) || !isset($query->query['page']))
        return;

    if (!empty($query->query['name']))
        $query->set('post_type', ['post', 'regionais', 'page']);
}
add_action('pre_get_posts', 'setEmptyQueries');

/**
 * Change regional slug before save
 *
 * @param array $data
 * @param array $postarr
 * @return array
 */
function editRegionalSlug(array $data, array $postarr): array
{
    $title = trim($data['post_title']);

    if (in_array($title, array_keys(GLOBALS['STATES'])))
        $data['post_name'] = strtolower(GLOBALS['STATES'][$title]);

    return $data;
}
add_filter('wp_insert_post_data', 'editRegionalSlug', 99, 2);

/**
 * Change some archive page title
 *
 * @param string $title
 * @return string
 */
function customArchiveTitles(string $title): string
{
    return $title;
}
add_filter('get_the_archive_title', 'customArchiveTitles');

/**
 * Custom admin sidebar labels
 *
 * @return void
 */
function customAdminLabels(): void
{
    global $menu;
    $menu[70][0] = 'Usuários e Associados';
}
add_action('admin_menu', 'customAdminLabels', 99);

/**
 * Send an welcome email when user becomes a member
 *
 * @param int $userId,
 * @param string $role
 * @param array $oldRoles
 * @return void
 */
function sendEmailToNewMember(int $userId, string $role, array $oldRoles): void
{
    if ($role === 'membro') {
        $user = get_user_by('ID', $userId);

        $email = $user->data->user_email;
        $name = $user->data->display_name;
        $login = $user->data->user_login;
        $password = sanitize_title($login);

        wp_update_user(['ID' => $userId, 'user_pass' => $password]);

        sendMailMember($name, $email, $login, $password);
    }
}
add_action('set_user_role', 'sendEmailToNewMember', 10, 3);

/**
 * Format URL and message for reset password e-mail
 *
 * @param string|null $message
 * @param string $key
 * @param string $user_login
 * @param WP_User $user_data
 * @return string
 */
function customPasswordReset($message, string $key, string $user_login, WP_User $user_data): string
{
    $address = network_site_url(
        "wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login),
        'login'
    ) . "\r\n";

    $message = "Olá, para recuperar sua senha, acesse o seguinte endereço: $address";

    return $message;
}
add_filter('retrieve_password_message', 'customPasswordReset', 99, 4);

/**
 * Hide wordpress/plugins notifications for all users
 *
 * @return object
 */
function disableNotifications(): object
{
    global $wp_version;
    return (object) [
        'last_checked' => time(),
        'version_checked' => $wp_version
    ];
}
add_filter('pre_site_transient_update_core', 'disableNotifications');
add_filter('pre_site_transient_update_plugins', 'disableNotifications');
add_filter('pre_site_transient_update_themes', 'disableNotifications');

/**
 * Handle user/member information before its update
 *
 * @param int $userId
 * @param WP_User $oldUserData
 * @return void
 */
function beforeUpdateUserProfile(int $userId, WP_User $oldUserData): void
{
    /**
     * Modify youtube original url to embed url (to work in iframe tag)
     */
    $videoUrl = get_user_meta($userId, 'video_membro', true);

    if (!strpos($videoUrl, 'watch'))
        return;

    $parsedUrl = parse_url($videoUrl);
    parse_str($parsedUrl['query'], $query);
    $newVideoUrl = "{$parsedUrl['scheme']}://{$parsedUrl['host']}/embed/{$query['v']}";
    update_user_meta($userId, 'video_membro', $newVideoUrl);
}
add_action('profile_update', 'beforeUpdateUserProfile', 10, 2);

/**
 * Hide fields from admin panel (for non-admins users)
 *
 * @return void
 */
function hideCustomFieldsPostbox(): void
{
  if (is_admin() && in_array('membro', wp_get_current_user()->roles)) {
    ?>
        <style type="text/css">
            tr.user-display-name-wrap *,
            tr.user-language-wrap *,
            tr.user-admin-bar-front-wrap *,
            tr.user-sessions-wrap *,
            tr.user-nickname-wrap *,
            tr.user-description-wrap *,
            tr.user-profile-picture *,
            tr.user-pinterest-wrap *,
            tr.user-soundcloud-wrap *,
            tr.user-tumblr-wrap *,
            tr.user-twitter-wrap *,
            tr.user-youtube-wrap *,
            tr.user-wikipedia-wrap *,
            tr.user-myspace-wrap *,
            tr[data-name=nome_da_regional] *,
            tr[data-name=mensalidade_membro] *,
            tr[data-name=cargo_membro] *,
            tr[data-name=tipo_membro] *,
            tr[data-name=cnpj_membro] *,
            tr[data-name=representanteLegal_membro] *,
            tr[data-name=emailCobranca_membro] *,
            tr[data-name=cep_membro] *,
            tr[data-name=celular_membro] *,
            tr[data-name=site_membro] *,
            tr[data-name=contato_membro] *,
            tr[data-name=nomeFantasia_membro] *,
            tr[data-name=endereco_membro] *,
            tr[data-name=complemento_membro] *,
            tr[data-name=cidade_membro] *,
            tr[data-name=estado_membro] *,
            tr[data-name=facebook_membro] *,
            tr[data-name=twitter_membro] * {
                display: none !important;
                visibility: hidden !important;
                color: transparent !important;
            }
        </style>
    <?php
  }
}
add_action('admin_head', 'hideCustomFieldsPostbox');

/**
 * Remove last "/" from strings
 *
 * @param string $info
 * @return string
 */
function removeSlashBar(string $info): string
{
    if (substr($info, -1) === '/')
        return substr($info, 0, -1);

    return $info;
}

/**
 * Remove unnecessary attributes from script and link tags (JS/CSS)
 *
 * @return void
 */
function addSupportForTheme(): void
{
    add_theme_support('html5', ['script', 'style']);
}
add_action('after_setup_theme', 'addSupportForTheme');

/**
 * Create pagination link for archives and pages
 *
 * @param WP_Query|WP_User_Query $query
 * @param string $url
 * @param int $perPage
 * @return string|void
 */
function paginationLinks($query, int $perPage = 16)
{
    $totalPages = $query->max_num_pages ?? ceil($query->get_total() / $perPage);

    if ($totalPages > 1) {
        $big = 999999999;
        $currentPage = max(1, get_query_var('paged'));

        return paginate_links([
            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'show_all' => false,
            'format' => '?paged=%#%',
            'current' => $currentPage,
            'total' => $totalPages,
            'prev_text' => "Anterior",
            'next_text' => "Próximo",
        ]);
    }
}

/**
 * Create pagination linkS for search.php
 *
 * @return string|void
 */
function searchPaginationLinks()
{
    global $wp_query;

    $wp_query->query_vars['paged'] > 1
        ? $current = $wp_query->query_vars['paged']
        : $current = 1;

    return paginate_links([
        'base' => add_query_arg('paged','%#%'),
        'show_all' => false,
        'format' => '?paged=%#%',
        'current' => $current,
        'total' => $wp_query->max_num_pages,
        'prev_text' => "Anterior",
        'next_text' => "Próximo",
    ]);
}

/**
 * Fix some yoast breadcrumb links
 *
 * @param array $links
 * @return array
 */
function getBreadcrumbsLinks(array $links): array
{
    $regionalSlug = get_post($_SESSION['regional'])->post_name;
    $regionalTitle = get_the_title($_SESSION['regional']);

    // index/single-regionais link
    $links[0] = [
        'text' => "Home $regionalTitle",
        'url' => home_url("/$regionalSlug"),
        'allow_html' => true,
    ];

    // author page
    if (is_author()) {
        $breadcrumb[] = [
            'text' => 'Agentes Digitais',
            'url' => home_url('/agentes-digitais'),
            'allow_html' => true,
        ];

        array_splice($links, 1, -2, $breadcrumb);
    }

    return $links;
}
add_filter('wpseo_breadcrumb_links', 'getBreadcrumbsLinks');

/**
 * REST API start
 */
add_action('rest_api_init', 'routes');

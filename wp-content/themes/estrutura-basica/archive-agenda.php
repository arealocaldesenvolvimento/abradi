<?php

get_header();

$eventSearchQuery = $_GET['evento'] ?? '';
$states = GLOBALS['STATES'];
unset($states['Acre'], $states['Alagoas'], $states['Amapá'], $states['Amazonas'], $states['Bahia'], $states['Ceará'], $states['Espírito Santo'], $states['Maranhão'], $states['Mato Grosso'], $states['Minas Gerais'], $states['Pará'], $states['Pernambuco'], $states['Piauí'], $states['Rondônia'], $states['Roraima'], $states['Sergipe'], $states['Tocantins']);

?>

    <link
        media="all"
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/tail.datetime@0.4.13/css/tail.datetime-default-blue.min.css"
    >
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        rel="stylesheet"
    >
    <section class="section-eventos">
        <div class="al-small-container">
            <div class="eventos-search-bar">
                <nav class="breadcrumb">
                    <?= yoast_breadcrumb() ?>
                </nav>
                <h1 class="section-title-01">Eventos</h1>
                <p>Você está na ABRADi: <span><?= get_post($_SESSION['regional'])->post_title ?></span></p>
                <form method="GET" role="search">
                    <label>
                        <input
                            type="text"
                            class="calendar-01"
                            name="de"
                            placeholder=" EVENTOS DE:"
                            autocomplete="off"
                        >
                    </label>
                    <label>
                        <input
                            type="text"
                            class="calendar-02"
                            name="ate"
                            placeholder=" EVENTOS ATÉ:"
                            autocomplete="off"
                        >
                    </label>
                    <select name="estado">
                        <option value="">Todos</option>
                        <?php foreach ($states as $key => $state): ?>
                            <option value="<?= $key ?>">
                                <?= $state ?>
                            </option>
                        <?php endforeach ?>
                    </select>
                    <label>
                        <input
                            type="text"
                            name="evento"
                            placeholder="NOME DO EVENTO"
                        >
                    </label>
                    <button type="submit">Pesquisar</button>
                </form>
            </div>
            <div class="eventos-recentes">
                <?php $events = pageEventosQuery($eventSearchQuery) ?>
                <?php if ($events->results->have_posts()): $count = 1 ?>
                    <?php while ($events->results->have_posts()): $events->results->the_post() ?>
                        <?php
                            $title = get_the_title();
                            $permalink = sanitizeString(get_the_permalink());
                            $date = get_field('data');
                            $linkGoogleCalendar = formatGoogleCalendar(
                                $title,
                                $date,
                                $date,
                                sanitizeString(get_field('nome_do_local_do_evento')),
                                sanitizeString(get_the_excerpt())
                            );
                            $year = substr($date, 0, 4);
                            $month = substr($date, 4, 2);
                            $day = substr($date, 6, 2);
                        ?>
                        <?php if ($count !== 1): ?>
                            <article class="evento">
                                <div class="infos">
                                    <span class="regional-label">
                                        <?= get_the_terms(get_the_ID(), 'regionais')[0]->name ?>
                                    </span>
                                    <a
                                        href="<?= $linkGoogleCalendar ?>"
                                        class="calendar-label"
                                        title="Adicionar evento à agenda"
                                        target="_blank"
                                    >
                                        Coloque na Agenda
                                    </a>
                                </div>
                                <a href="<?= $permalink ?>">
                                    <?php if (get_the_post_thumbnail_url()): ?>
                                        <img
                                            src="<?= get_the_post_thumbnail_url(null, GLOBALS['THUMB_SIZE']) ?>"
                                            alt="<?= get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ?>"
                                            title="<?= $title ?>"
                                        />
                                    <?php else: ?>
                                        <img
                                            src="<?= get_image_url('default-event-thumbnail.jpg') ?>"
                                            alt='Imagem destacada padrão para eventos'
                                            title="<?= $title ?>"
                                        />
                                    <?php endif ?>
                                </a>
                                <div class="content">
                                    <a href="<?= $permalink ?>">
                                        <div class="date">
                                            <span class="day"><?= $day ?></span>
                                            <p>
                                                <span class="month">
                                                    <?= strtolower(getMonthName($month)) ?>
                                                </span>
                                                <span class="year"><?= $year ?></span>
                                            </p>
                                        </div>
                                    </a>
                                    <a href="<?= $permalink ?>" title="<?= $title ?>" class="title">
                                        <h2><?= $title ?></h2>
                                    </a>
                                </div>
                            </article>
                        <?php else: ?>
                            <?php
                                $eventSite = sanitizeString(get_field('site_organizacao'));
                                $placeSite = sanitizeString(get_field('site_do_local'));
                                $address = sanitizeString(get_field('endereco_local'));
                            ?>
                            <article class="evento highlight">
                                <span class="regional-label">
                                    <?= get_the_terms(get_the_ID(), 'regionais')[0]->name ?>
                                </span>
                                <a
                                    href="<?= $linkGoogleCalendar ?>"
                                    class="calendar-label"
                                    title="Adicionar evento à agenda"
                                    target="_blank"
                                >
                                    Coloque na Agenda
                                </a>
                                <a href="<?= $permalink ?>">
                                    <?php if (get_the_post_thumbnail_url()): ?>
                                        <img
                                            src="<?= get_the_post_thumbnail_url(null, GLOBALS['THUMB_SIZE']) ?>"
                                            alt="<?= get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ?>"
                                            title="<?= $title ?>"
                                        />
                                    <?php else: ?>
                                        <img
                                            src="<?= get_image_url('default-event-thumbnail.jpg') ?>"
                                            alt='Imagem destacada padrão para eventos'
                                            title="<?= $title ?>"
                                        />
                                    <?php endif ?>
                                </a>
                                <div class="content">
                                    <a href="<?= $permalink ?>">
                                        <div class="date">
                                            <span class="day"><?= $day ?></span>
                                            <p>
                                                <span class="month">
                                                    <?= strtolower(getMonthName($month)) ?>
                                                </span>
                                                <span class="year"><?= $year ?></span>
                                            </p>
                                        </div>
                                    </a>
                                    <a href="<?= $permalink ?>" title="<?= $title ?>" class="title">
                                        <h2><?= $title ?></h2>
                                    </a>
                                    <?php if ($eventSite): ?>
                                        <a href="<?= $permalink ?>" class="site">
                                            <p>
                                                <img
                                                    class="ingresso"
                                                    src="<?= get_image_url('eventos/ingresso.svg') ?>"
                                                    alt="Ingresso"
                                                    title="Ingresso"
                                                />
                                                <?= sanitizeString($eventSite) ?>
                                            </p>
                                        </a>
                                    <?php endif ?>
                                    <?php if ($placeSite): ?>
                                        <a href="<?= $permalink ?>" class="site_do_local">
                                            <p>
                                                <img
                                                    src="<?= get_image_url('eventos/globo.svg') ?>"
                                                    alt="Globo"
                                                    title="Globo"
                                                />
                                                <?= sanitizeString($placeSite) ?>
                                            </p>
                                        </a>
                                    <?php endif ?>
                                    <?php if ($address): ?>
                                        <a href="<?= $permalink ?>" class="endereco">
                                            <p>
                                                <img
                                                    src="<?= get_image_url('eventos/maps.svg') ?>"
                                                    alt="Endereço"
                                                    title="Endereço"
                                                />
                                                <?= sanitizeString($address) ?>
                                            </p>
                                        </a>
                                    <?php endif ?>
                                    <a
                                        href="<?= $linkGoogleCalendar ?>"
                                        class="agenda"
                                        title="Adicionar evento à agenda"
                                        target="_blank"
                                    >
                                        Coloque na Agenda
                                    </a>
                                </div>
                            </article>
                        <?php endif ?>
                    <?php $count ++; endwhile ?>
                <?php else: ?>
                    <div>
                        <p class="notfound-message">Não foram encontrados eventos para essa pesquisa</p>
                    </div>
                <?php endif ?>
            </div>
            <div>
                <nav class="pagination">
                    <?= paginationLinks($events->results, $events->perPage) ?>
                </nav>
            </div>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/tail.datetime@0.4.13/js/tail.datetime.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/tail.datetime@0.4.13/langs/tail.datetime-pt_BR.js" defer></script>
    <script src="<?= get_template_directory_uri() ?>/public/archive-agenda.js" defer></script>

<?php get_footer() ?>

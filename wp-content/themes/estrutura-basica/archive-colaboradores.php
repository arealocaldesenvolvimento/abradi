<?php

get_header();

// Page Cplaboradores ID
$post = get_post(24);

?>

    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css"
        rel="stylesheet"
    >
    <section class="section-colaboradores">
        <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
        </nav>
        <div class="al-small-container">
            <h1 class="section-title-01">Colaboradores</h1>
            <p><?= apply_filters('the_content', $post->post_content) ?></p>
            <?php $contributors = archiveColaboradoresQuery(get_post($_SESSION['regional'])->post_name) ?>
            <?php if ($contributors->have_posts()): ?>
                <div id="slider-patrocinadores">
                    <?php while ($contributors->have_posts()): $contributors->the_post() ?>
                        <?php $alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ?>
                        <a
                            class="patrocinador"
                            href="<?= get_field('link') ?>"
                            target="_blank"
                        >
                            <img
                                src="<?= get_the_post_thumbnail_url(null, GLOBALS['THUMB_SIZE']) ?>"
                                alt="<?= $alt ?>"
                                <?php if (!empty($imagem = get_field('imagem_preto_branco'))): ?>
                                    onmouseover="this.src='<?= $imagem['sizes']['large'] ?>'"
                                    onmouseleave="this.src='<?= get_the_post_thumbnail_url(null, GLOBALS['THUMB_SIZE']) ?>'"
                                <?php endif ?>
                            />
                        </a>
                    <?php endwhile ?>
                </div>
            <?php else: ?>
                <div style="margin-top: 20px">
                    <p class="notfound-message">Não foram cadastrados colaboradores</p>
                </div>
            <?php endif ?>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/tiny-slider.js"></script>
    <script src="<?= get_template_directory_uri() ?>/public/archive-colaboradores.js" defer></script>

<?php get_footer() ?>


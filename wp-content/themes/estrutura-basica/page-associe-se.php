<?php get_header() ?>

    <section class="section-associe-se">
        <div class="al-small-container">
            <div>
                <nav class="breadcrumb">
                    <?= yoast_breadcrumb() ?>
                </nav>
                <h1 class="section-title-01">Seja associado da ABRADi</h1>
            </div>
            <!-- Form Associe-se -->
            <form name="form-associe-se" role="form">
                <input type="hidden" name="currentGroup" value="1">
                <input type="hidden" name="tipoEmpresa" value="Agência Digital">
                <input type="hidden" name="mensalidade" value="Grupo 1 - R$108">
                <div class="group-1 current">
                    <!-- Tipo -->
                    <div>
                        <label>Tipo <span>(campo obrigatório)</span></label>
                        <select name="tipo" required>
                            <option value="0">Agência Digital</option>
                            <option value="1">Produtora Digital</option>
                            <option value="2">Agente Digital</option>
                            <option value="3">Colaborador</option>
                            <option value="4">Agente Apoiador</option>
                        </select>
                    </div>
                    <!-- Empresa -->
                    <div>
                        <label>Razão Social <span>(campo obrigatório)</span></label>
                        <input type="text" name="empresa" required>
                    </div>
                    <!-- Nome Fantasia -->
                    <div>
                        <label>Nome Fantasia <span>(campo obrigatório - nome e sobrenome)</span></label>
                        <input type="text" name="nomeFantasia" required>
                    </div>
                    <!-- CNPJ -->
                    <div>
                        <label>CNPJ/MF <span>(campo obrigatório)</span></label>
                        <input type="text" name="cnpj" required>
                    </div>
                </div>
                <div class="group-2">
                    <!-- Endereço -->
                    <div>
                        <label>Endereço/Número <span>(campo obrigatório)</span></label>
                        <input type="text" name="endereco" required>
                    </div>
                    <!-- Complemento -->
                    <div>
                        <label>Complemento <span>(campo obrigatório)</span></label>
                        <input type="text" name="complemento" required>
                    </div>
                    <!-- UF -->
                    <div>
                        <label>UF <span>(campo obrigatório)</span></label>
                        <select name="estado" required>
                            <option selected disabled>Estado</option>
                        </select>
                    </div>
                    <!-- Cidade -->
                    <div>
                        <label>Cidade <span>(campo obrigatório)</span></label>
                        <select name="cidade" required>
                            <option selected disabled>Cidade</option>
                        </select>
                    </div>
                    <!-- CEP -->
                    <div>
                        <label>CEP <span>(campo obrigatório)</span></label>
                        <input type="text" name="cep" required>
                    </div>
                </div>
                <div class="group-3">
                    <!-- E-mail -->
                    <div>
                        <label>E-mail <span>(campo obrigatório)</span></label>
                        <input type="email" name="email" required>
                    </div>
                    <!-- Website -->
                    <div>
                        <label>Website <span>(campo obrigatório)</span></label>
                        <input type="text" name="site" required>
                    </div>
                    <!-- Telefone comercial -->
                    <div>
                        <label>Telefone comercial <span>(campo obrigatório)</span></label>
                        <input type="text" name="telefone" required>
                    </div>
                    <!-- Faixa de Faturamento -->
                    <div>
                        <label>Faixa de Faturamento <span>(campo obrigatório)</span></label>
                        <select name="faixaFaturamento" required>
                            <option value="0" selected>até 540.000,00 - Mensalidade: R$108</option>
                            <option value="1">540.000,01 a 1.080.000,00 - Mensalidade: R$129</option>
                            <option value="2">1.080.000,01 a 2.160.000,00 - Mensalidade: R$162</option>
                            <option value="3">2.160.000,01 a 3.235.000,00 - Mensalidade: R$194</option>
                            <option value="4">3.235.000,01 a 5.400.000,00 - Mensalidade: R$270</option>
                            <option value="5">5.400.000,01 a 8.625.000,00 - Mensalidade: R$400</option>
                            <option value="6">8.625.000,01 a 12.940.000,00 - Mensalidade: R$600</option>
                            <option value="7">acima de 12.940.000,00 - Mensalidade: R$750</option>
                        </select>
                    </div>
                </div>
                <div class="group-4">
                    <!-- Representante Legal -->
                    <div>
                        <label>Representante Legal <span>(campo obrigatório) (nome e sobrenome)</span></label>
                        <input
                            type="text"
                            name="representanteLegal"
                            pattern="[a-zA-Z][a-zA-Z ]{4,}"
                            oninput="this.value = this.value.replace(/[^a-zA-Z, ]/, '')"
                            required
                        >
                    </div>
                    <!-- Cargo -->
                    <div>
                        <label>Cargo <span>(campo obrigatório)</span></label>
                        <input type="text" name="cargo" required>
                    </div>
                    <!-- E-mail de Cobrança -->
                    <div>
                        <label>E-mail de Cobrança <span>(campo obrigatório)</span></label>
                        <input type="email" name="emailCobranca" required>
                    </div>
                    <!-- Telefone Celular -->
                    <div>
                        <label>Telefone Celular <span>(campo obrigatório)</span></label>
                        <input type="text" name="celular" required>
                    </div>
                </div>
                <!-- Actions -->
                <div class="actions">
                    <button type="button" name="previous" disabled>Voltar</button>
                    <button type="button" name="next">Avançar</button>
                    <input type="submit" value="Enviar!">
                </div>
            </form>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/imask.js"></script>
    <script src="<?= get_template_directory_uri() ?>/public/sweetalert2@9.js"></script>
    <script src="<?= get_template_directory_uri() ?>/public/page-associe-se.js" defer></script>

<?php get_footer() ?>

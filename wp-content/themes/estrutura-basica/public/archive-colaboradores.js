/**
 * @description Slider patrocinadores
 */
tns({
	container: '#slider-patrocinadores',
	items: 1,
	loop: true,
	mouseDrag: true,
	touch: true,
	arrowKeys: false,
	controls: false,
	nav: false,
	autoplay: true,
	speed: 600,
	responsive: {
		1024: {
			items: 4,
		},
		769: {
			items: 3,
		},
		480: {
			items: 2,
			gutter: 50,
		},
	},
})

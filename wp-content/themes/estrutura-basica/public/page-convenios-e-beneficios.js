/**
 * @description Agreements cards dropdown
 */
const agreements = document.querySelectorAll(
	'section.section-convenios-e-beneficios .convenio-wrapper'
)

if (agreements !== null) {
	agreements.forEach(agreement => {
		agreement.querySelector('header').addEventListener('click', () => {
			agreement.classList.toggle('active')
		})
	})
}

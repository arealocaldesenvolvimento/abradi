/**
 * @description Input masks
 */
IMask(document.querySelector('input[name=telefone]'), {
	mask: '(00) 0000-0000',
})

/**
 * @description Put regional contact e-mail in contact-form hidden input
 */
const emailCustomField = document.getElementById('email-regional').value
document.querySelector('input[name=email-regional]').value = emailCustomField

/**
 * @description Map render
 *
 * @param {string} lat
 * @param {string} lng
 */
function maps(lat, lng) {
	let templateUrl = window.alUrl.templateUrl
	const markerUrl = templateUrl + '/assets/images/cd-icon-location.png'
	const mapZoom = 14

	const mapOptions = {
		center: new google.maps.LatLng(lat, lng),
		zoom: mapZoom,
		panControl: false,
		zoomControl: false,
		mapTypeControl: false,
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
	}

	const map = new google.maps.Map(
		document.querySelector('#google-container'),
		mapOptions
	)

	new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: map,
		visible: true,
		icon: markerUrl,
	})

	function CustomZoomControl(controlDiv, map) {
		let controlUIzoomIn = document.getElementById('cd-zoom-in')
		let controlUIzoomOut = document.getElementById('cd-zoom-out')

		controlDiv.appendChild(controlUIzoomIn)
		controlDiv.appendChild(controlUIzoomOut)

		google.maps.event.addDomListener(controlUIzoomIn, 'click', () => {
			map.setZoom(map.getZoom() + 1)
		})
		google.maps.event.addDomListener(controlUIzoomOut, 'click', () => {
			map.setZoom(map.getZoom() - 1)
		})
	}

	let zoomControlDiv = document.createElement('div')
	new CustomZoomControl(zoomControlDiv, map)

	map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv)
}

const latitude = document.querySelector('.lat')
const longitude = document.querySelector('.lng')

if (latitude !== null && longitude !== null) {
	maps(latitude.value, longitude.value)
}

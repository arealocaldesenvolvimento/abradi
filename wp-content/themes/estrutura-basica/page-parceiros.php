<?php get_header() ?>

    <section class="section-materiais">
        <div class="al-small-container">
            <div class="materiais">
                <nav class="breadcrumb">
                    <?= yoast_breadcrumb() ?>
                </nav>
                <h1 class="section-title-01">Materiais de parceiros</h1>
                <?php $materials = pageMateriaisParceirosQuery() ?>
                <?php if ($materials->have_posts()): ?>
                    <div>
                        <?php while ($materials->have_posts()): $materials->the_post() ?>
                            <article>
                                <?= setPostThumbnail() ?>
                                <div class="content">
                                    <h2><?= get_the_title() ?></h2>
                                    <a href="<?= get_field('link') ?>" target="_blank">
                                        Saber mais!
                                        <img
                                            src="<?= get_image_url('flechas/flecha-direita-branca-circulo.svg') ?>"
                                            alt="Flecha para direita"
                                        >
                                    </a>
                                </div>
                            </article>
                        <?php endwhile ?>
                    </div>
                <?php else: ?>
                    <div>
                        <p class="notfound-message">Não foram encontrados materiais</p>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </section>

<?php get_footer() ?>

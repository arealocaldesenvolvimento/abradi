<?php get_header() ?>

    <section class="section-convenios-e-beneficios">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <h1 class="section-title-01">Convênios e Benefícios</h1>
            <div class="main-content">
                <p>Vantagens exclusivas e TODOS associados – Nacional e Regionais, podem usufruir dos benefícios oferecidos. São diversas empresas oferecendo descontos e benefícios em ferramentas, softwares, pesquisas, serviços e cursos. Se a sua empresa quer ser conveniada à ABRADi, envie e-mail para:  abradi@abradi.com.br.</p>
                <div class="cta">
                    <h2>Que tal usufruir desses benefícios?</h2>
                    <a href="<?= get_permalink(38) ?>" title="Associe-se!" target="_blank">
                        Seja um Associado!
                    </a>
                </div>
            </div>
            <div class="convenios-e-beneficios">
                <?php foreach (get_terms('categoria_convenio', ['hide_empty' => false]) as $term): ?>
                    <?php $convenios = pageConveniosEBeneficiosQuery($term) ?>
                    <?php if ($convenios->have_posts()): ?>
                        <?php
                            $id = $term->taxonomy . '_' . $term->term_id;
                            $count = $convenios->found_posts;
                            $sectionTitle = $term->name;
                        ?>
                        <section class="convenio-wrapper">
                            <header>
                                <img
                                    class="icone"
                                    src="<?= get_field('icone', $id)['sizes']['medium'] ?>"
                                    alt="<?= $sectionTitle ?>"
                                />
                                <div class="content">
                                    <h2><?= $sectionTitle ?></h2>
                                    <p>
                                        <?= $count >= 10 ? $count : str_pad($count, 2, '0', STR_PAD_LEFT) ?>
                                        Convênios
                                    </p>
                                </div>
                                <img
                                    class="seta"
                                    src="<?= get_image_url('convenios-e-beneficios/seta.png') ?>"
                                    alt="Seta para cima"
                                />
                            </header>
                            <?php while ($convenios->have_posts()): $convenios->the_post() ?>
                                <?php $permalink = get_the_permalink() ?>
                                <article class="convenio">
                                    <header>
                                        <h3 class="section-title-01"><?= get_the_title() ?></h3>
                                        <?php if ($coverage = get_field('abrangencia_convenio')): ?>
                                            <p>Abrangência: <?= $coverage ?></p>
                                        <?php endif ?>
                                    </header>
                                    <div class="content">
                                        <?php if ($logo = get_field('logo')): ?>
                                            <?php if (is_array($logo)): ?>
                                                <?= wp_get_attachment_image($logo['ID'], 'medium') ?>
                                            <?php elseif (wp_get_attachment_image($logo, 'medium')): ?>
                                                <?= wp_get_attachment_image($logo, 'medium') ?>
                                            <?php else: ?>
                                                <img
                                                    src="<?= get_image_url('default-thumbnail.jpg') ?>"
                                                    alt="Imagem destacada padrão para postagens"
                                                />
                                            <?php endif ?>
                                        <?php else: ?>
                                            <img
                                                src="<?= get_image_url('default-thumbnail.jpg') ?>"
                                                alt="Imagem destacada padrão para postagens"
                                            />
                                        <?php endif ?>
                                        <div class="text">
                                            <p><?= wpautop(sanitizeString(get_the_content())) ?></p>
                                            <?php if (get_field('solicitar_beneficio_convenio')): ?>
                                                <a
                                                    href="<?= get_permalink(38) ?>"
                                                    title="Solicite seu Benefício!"
                                                    target="_blank"
                                                >
                                                    Solicitar seu Benefício
                                                </a>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </article>
                            <?php endwhile ?>
                        </section>
                    <?php endif ?>
                <?php endforeach ?>
            </div>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/page-convenios-e-beneficios.js" defer></script>

<?php get_footer() ?>


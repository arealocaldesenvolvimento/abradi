<?php get_header() ?>

    <section class="section-single-eventos">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <div class="evento">
                <?php while (have_posts()): the_post() ?>
                    <?php
                        $title = get_the_title();
                        $permalink = sanitizeString(get_the_permalink());
                        $date = get_field('data');
                        $eventSite = sanitizeString(get_field('site_organizacao'));
                        $placeSite = sanitizeString(get_field('site_do_local'));
                        $address = sanitizeString(get_field('endereco_local'));
                        $location = get_field('localizacao');
                    ?>
                    <h1 class="section-title-01"><?= $title ?></h1>
                    <?php if (get_the_post_thumbnail_url()): ?>
                        <img
                            src="<?= get_the_post_thumbnail_url(null, GLOBALS['THUMB_SIZE']) ?>"
                            alt="<?= get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ?>"
                            title="<?= $title ?>"
                        />
                    <?php else: ?>
                        <img
                            src="<?= get_image_url('default-event-thumbnail.jpg') ?>"
                            alt='Imagem destacada padrão para eventos ABRADi'
                            title="<?= $title ?>"
                        />
                    <?php endif ?>
                    <div>
                        <?php if (get_the_content()): ?>
                            <h2 class="section-title-01">Descrição</h2>
                        <?php endif ?>
                        <div class="content">
                            <?php if (get_the_content()): ?>
                                <div class="text">
                                    <?php the_content() ?>
                                </div>
                            <?php endif ?>
                            <div class="calendar-link">
                                <p>Não fique de fora dessa e</p>
                                <a
                                    href="<?=
                                        formatGoogleCalendar(
                                            $title,
                                            $date,
                                            $date,
                                            sanitizeString(get_field('nome_do_local_do_evento')),
                                            sanitizeString(get_the_excerpt())
                                        )
                                    ?>"
                                    class="agenda"
                                    title="Adicionar evento à agenda"
                                    target="_blank"
                                >
                                    Coloque na Agenda
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="detalhes">
                        <div>
                            <h2 class="section-title-01">Detalhes do Evento</h2>
                            <ul>
                                <?php if ($date): ?>
                                    <li>
                                        <p class="data">
                                            <img
                                                src="<?= get_image_url('single-eventos/calendario-azul.svg') ?>"
                                                alt="Data"
                                                title="Data"
                                            />
                                            <?= date('d/m/Y', strtotime($date)) ?>
                                        </p>
                                    </li>
                                <?php endif ?>
                                <?php if ($placeSite): ?>
                                    <li>
                                        <p>
                                            <img
                                                src="<?= get_image_url('single-eventos/ingresso-azul.svg') ?>"
                                                alt="Local"
                                                title="Local"
                                            />
                                            <a
                                                href="<?= $placeSite ?>"
                                                title="Site do local do evento"
                                                target="_blank"
                                            >
                                                <?= $placeSite ?>
                                            </a>
                                        </p>
                                    </li>
                                <?php endif ?>
                                <?php if ($eventSite): ?>
                                    <li>
                                        <p>
                                            <img
                                                src="<?= get_image_url('single-eventos/globo-azul.svg') ?>"
                                                alt="Organização"
                                                title="Organização"
                                            />
                                            <a href="<?= $eventSite ?>" title="Site do evento" target="_blank">
                                                <?= $eventSite ?>
                                            </a>
                                        </p>
                                    </li>
                                <?php endif ?>
                            </ul>
                        </div>
                        <div>
                            <h2 class="section-title-01">Organização</h2>
                            <strong><?= sanitizeString(get_field('organizacao')) ?></strong>
                            <?php if ($eventSite): ?>
                                <p>
                                    <img
                                        src="<?= get_image_url('single-eventos/globo-azul.svg') ?>"
                                        alt="Organização"
                                        title="Organização"
                                    />
                                    <a href="<?= $eventSite ?>" title="Site do evento" target="_blank">
                                        <?= $eventSite ?>
                                    </a>
                                </p>
                            <?php endif ?>
                        </div>
                    </div>
                    <?php if ($location && $address): ?>
                        <div class="localizacao">
                            <h2 class="section-title-01">Localização</h2>
                            <div class="infos">
                                <p>
                                    <img
                                        src="<?= get_image_url('single-eventos/maps-azul.svg') ?>"
                                        alt="Localização"
                                        title="Localização"
                                    />
                                    <?= $address ?>
                                </p>
                                <a
                                    href="http://maps.google.com/?q=<?= $address ?>"
                                    title="Como Chegar?"
                                    target="_blank"
                                >
                                    Como Chegar?
                                </a>
                            </div>
                            <div class="mapa">
                                <input class="lat" type="hidden" value="<?= $location['lat'] ?>">
                                <input class="lng" type="hidden" value="<?= $location['lng'] ?>">
                                <section id="cd-google-map">
                                    <div id="google-container"></div>
                                    <div id="cd-zoom-in"></div>
                                    <div id="cd-zoom-out"></div>
                                </section>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endwhile ?>
            </div>
        </div>
    </section>

    <!-- Scripts -->
    <script src="<?= get_template_directory_uri() ?>/public/single-agenda.js" defer></script>

<?php get_footer() ?>


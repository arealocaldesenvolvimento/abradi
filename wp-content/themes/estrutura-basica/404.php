<?php get_header() ?>

    <section class="section-404">
        <div class="al-small-container">
            <header class="content">
                <h1 class="section-title-01">Erro 404</h1>
                <p>A página não foi encontrada, faça uma busca no site ou entre em contato conosco.</p>
            </header>
        </div>
    </section>

<?php get_footer() ?>

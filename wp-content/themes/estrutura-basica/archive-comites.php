<?php

get_header();

// Page Comites ID
$post = get_post(90);

?>

    <section class="section-comites">
        <div class="al-small-container">
            <nav class="breadcrumb">
                <?= yoast_breadcrumb() ?>
            </nav>
            <h1 class="section-title-01">Comitês</h1>
            <p><?= apply_filters('get_the_content', $post->post_content) ?></p>
            <?php $comites = archiveComitesQuery() ?>
            <div class="comites">
                <?php if ($comites->have_posts()): ?>
                    <?php while ($comites->have_posts()): $comites->the_post() ?>
                        <article>
                            <a href="<?= get_permalink() ?>">
                                <?= setPostThumbnail() ?>
                            </a>
                            <div class="content">
                                <h2><?= get_the_title() ?></h2>
                                <p><?= sanitizeString(get_the_content()) ?></p>
                            </div>
                        </article>
                    <?php endwhile ?>
                <?php else: ?>
                    <p class="notfound-message">Não foram cadastrados comitês</p>
                <?php endif ?>
            </div>
        </div>
    </section>

<?php get_footer() ?>

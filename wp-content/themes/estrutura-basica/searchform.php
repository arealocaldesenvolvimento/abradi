<form id="search" role="search" method="GET" action="<?= home_url('/') ?>">
    <fieldset form="search">
        <label for="s">
            <p class="ases-text">Busca</p>
            <input
                id="s"
                type="search"
                name="s"
                value="<?= get_search_query() ?>"
                required
                placeholder="O que está procurando?"
            >
        </label>
        <button type="submit" form="search">
            <img src="<?= get_image_url('/header/search.svg') ?>" alt="Buscar" />
        </button>
    </fieldset>
</form>

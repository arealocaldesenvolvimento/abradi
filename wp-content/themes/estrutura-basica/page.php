<?php get_header() ?>

    <section class="al-small-container">
        <?php while (have_posts()): the_post() ?>
            <article id="id-<?php the_ID() ?>" <?php post_class() ?>>
                <h1 class="section-title-01"><?php the_title() ?></h1>

                <div class="entry-content">
                    <?php the_content() ?>
                </div>
            </article>
        <?php endwhile ?>
    </section>

<?php get_footer() ?>

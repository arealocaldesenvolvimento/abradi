<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'abradi');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', '127.0.0.1');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<D!IFhlS=d@n:W2x~qE5JTR}3$$:#.c7||0gj)h?nF/I4DIMM+5&3 ur[FEzU{#.');
define('SECURE_AUTH_KEY',  '+O$mx%v@-I6v)L9yDC!$((1#3K<jgH*8m$lIUTLo5w2{F?- 3stY=-8QiUl>jG G');
define('LOGGED_IN_KEY',    '#?$.;5EE<oC^U|zRysT{5>$(8q*2rtn3-wW3CplQjLk0vf5w4I%ZjazJ2)1~td44');
define('NONCE_KEY',        'Y=Y{%MrgNpT9WD(0[cISC-kk#UEP T{ ~O|EW]^<Z,{.XglM3hFIwp1$:P*>V.go');
define('AUTH_SALT',        'UR5OpN5Aa+HfRTM^S`sN2/?eb-VwZbfuR-hHs?:(p!AbSu>FB+&{~.60D9<0T*H]');
define('SECURE_AUTH_SALT', 'o#ZE;vg~5anyAQ5`6zv~w.I+?i-/OFuR]T]7A[4Eq`1=N3;ieMf G)<Hpcxi1Cua');
define('LOGGED_IN_SALT',   '(DX)aGI9o;{m!]./:h1.t(|/CJ3<n|oU7JAi<}b)7&<~-:YIK <}+2)xu|fKl@Ib');
define('NONCE_SALT',       'QC%0o3f$9yWUFD_FfGDGh8h3ahWC(t8{oDp,6lSjZET1CbA7e2oP*sGW-<yU$hi5');
/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'al_';

/**
 * Custom vars
 */
define('SITE_ROOT', __DIR__);

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD','direct');

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
